
cmake-build-release/software.elf:     file format elf32-littlearm


Disassembly of section .text:

080000bc <__udivsi3>:
 80000bc:	2900      	cmp	r1, #0
 80000be:	d034      	beq.n	800012a <.udivsi3_skip_div0_test+0x6a>

080000c0 <.udivsi3_skip_div0_test>:
 80000c0:	2301      	movs	r3, #1
 80000c2:	2200      	movs	r2, #0
 80000c4:	b410      	push	{r4}
 80000c6:	4288      	cmp	r0, r1
 80000c8:	d32c      	bcc.n	8000124 <.udivsi3_skip_div0_test+0x64>
 80000ca:	2401      	movs	r4, #1
 80000cc:	0724      	lsls	r4, r4, #28
 80000ce:	42a1      	cmp	r1, r4
 80000d0:	d204      	bcs.n	80000dc <.udivsi3_skip_div0_test+0x1c>
 80000d2:	4281      	cmp	r1, r0
 80000d4:	d202      	bcs.n	80000dc <.udivsi3_skip_div0_test+0x1c>
 80000d6:	0109      	lsls	r1, r1, #4
 80000d8:	011b      	lsls	r3, r3, #4
 80000da:	e7f8      	b.n	80000ce <.udivsi3_skip_div0_test+0xe>
 80000dc:	00e4      	lsls	r4, r4, #3
 80000de:	42a1      	cmp	r1, r4
 80000e0:	d204      	bcs.n	80000ec <.udivsi3_skip_div0_test+0x2c>
 80000e2:	4281      	cmp	r1, r0
 80000e4:	d202      	bcs.n	80000ec <.udivsi3_skip_div0_test+0x2c>
 80000e6:	0049      	lsls	r1, r1, #1
 80000e8:	005b      	lsls	r3, r3, #1
 80000ea:	e7f8      	b.n	80000de <.udivsi3_skip_div0_test+0x1e>
 80000ec:	4288      	cmp	r0, r1
 80000ee:	d301      	bcc.n	80000f4 <.udivsi3_skip_div0_test+0x34>
 80000f0:	1a40      	subs	r0, r0, r1
 80000f2:	431a      	orrs	r2, r3
 80000f4:	084c      	lsrs	r4, r1, #1
 80000f6:	42a0      	cmp	r0, r4
 80000f8:	d302      	bcc.n	8000100 <.udivsi3_skip_div0_test+0x40>
 80000fa:	1b00      	subs	r0, r0, r4
 80000fc:	085c      	lsrs	r4, r3, #1
 80000fe:	4322      	orrs	r2, r4
 8000100:	088c      	lsrs	r4, r1, #2
 8000102:	42a0      	cmp	r0, r4
 8000104:	d302      	bcc.n	800010c <.udivsi3_skip_div0_test+0x4c>
 8000106:	1b00      	subs	r0, r0, r4
 8000108:	089c      	lsrs	r4, r3, #2
 800010a:	4322      	orrs	r2, r4
 800010c:	08cc      	lsrs	r4, r1, #3
 800010e:	42a0      	cmp	r0, r4
 8000110:	d302      	bcc.n	8000118 <.udivsi3_skip_div0_test+0x58>
 8000112:	1b00      	subs	r0, r0, r4
 8000114:	08dc      	lsrs	r4, r3, #3
 8000116:	4322      	orrs	r2, r4
 8000118:	2800      	cmp	r0, #0
 800011a:	d003      	beq.n	8000124 <.udivsi3_skip_div0_test+0x64>
 800011c:	091b      	lsrs	r3, r3, #4
 800011e:	d001      	beq.n	8000124 <.udivsi3_skip_div0_test+0x64>
 8000120:	0909      	lsrs	r1, r1, #4
 8000122:	e7e3      	b.n	80000ec <.udivsi3_skip_div0_test+0x2c>
 8000124:	0010      	movs	r0, r2
 8000126:	bc10      	pop	{r4}
 8000128:	4770      	bx	lr
 800012a:	b501      	push	{r0, lr}
 800012c:	2000      	movs	r0, #0
 800012e:	f000 f80b 	bl	8000148 <__aeabi_idiv0>
 8000132:	bd02      	pop	{r1, pc}

08000134 <__aeabi_uidivmod>:
 8000134:	2900      	cmp	r1, #0
 8000136:	d0f8      	beq.n	800012a <.udivsi3_skip_div0_test+0x6a>
 8000138:	b503      	push	{r0, r1, lr}
 800013a:	f7ff ffc1 	bl	80000c0 <.udivsi3_skip_div0_test>
 800013e:	bc0e      	pop	{r1, r2, r3}
 8000140:	4342      	muls	r2, r0
 8000142:	1a89      	subs	r1, r1, r2
 8000144:	4718      	bx	r3
 8000146:	46c0      	nop			; (mov r8, r8)

08000148 <__aeabi_idiv0>:
 8000148:	4770      	bx	lr
 800014a:	46c0      	nop			; (mov r8, r8)

0800014c <__do_global_dtors_aux>:
 800014c:	b510      	push	{r4, lr}
 800014e:	4c06      	ldr	r4, [pc, #24]	; (8000168 <__do_global_dtors_aux+0x1c>)
 8000150:	7823      	ldrb	r3, [r4, #0]
 8000152:	2b00      	cmp	r3, #0
 8000154:	d107      	bne.n	8000166 <__do_global_dtors_aux+0x1a>
 8000156:	4b05      	ldr	r3, [pc, #20]	; (800016c <__do_global_dtors_aux+0x20>)
 8000158:	2b00      	cmp	r3, #0
 800015a:	d002      	beq.n	8000162 <__do_global_dtors_aux+0x16>
 800015c:	4804      	ldr	r0, [pc, #16]	; (8000170 <__do_global_dtors_aux+0x24>)
 800015e:	e000      	b.n	8000162 <__do_global_dtors_aux+0x16>
 8000160:	bf00      	nop
 8000162:	2301      	movs	r3, #1
 8000164:	7023      	strb	r3, [r4, #0]
 8000166:	bd10      	pop	{r4, pc}
 8000168:	2000043c 	.word	0x2000043c
 800016c:	00000000 	.word	0x00000000
 8000170:	0800120c 	.word	0x0800120c

08000174 <frame_dummy>:
 8000174:	4b04      	ldr	r3, [pc, #16]	; (8000188 <frame_dummy+0x14>)
 8000176:	b510      	push	{r4, lr}
 8000178:	2b00      	cmp	r3, #0
 800017a:	d003      	beq.n	8000184 <frame_dummy+0x10>
 800017c:	4903      	ldr	r1, [pc, #12]	; (800018c <frame_dummy+0x18>)
 800017e:	4804      	ldr	r0, [pc, #16]	; (8000190 <frame_dummy+0x1c>)
 8000180:	e000      	b.n	8000184 <frame_dummy+0x10>
 8000182:	bf00      	nop
 8000184:	bd10      	pop	{r4, pc}
 8000186:	46c0      	nop			; (mov r8, r8)
 8000188:	00000000 	.word	0x00000000
 800018c:	20000440 	.word	0x20000440
 8000190:	0800120c 	.word	0x0800120c

08000194 <SystemClock_Config>:
 8000194:	b500      	push	{lr}
 8000196:	b093      	sub	sp, #76	; 0x4c
 8000198:	2238      	movs	r2, #56	; 0x38
 800019a:	2100      	movs	r1, #0
 800019c:	a804      	add	r0, sp, #16
 800019e:	f000 ff79 	bl	8001094 <memset>
 80001a2:	2210      	movs	r2, #16
 80001a4:	2100      	movs	r1, #0
 80001a6:	4668      	mov	r0, sp
 80001a8:	f000 ff74 	bl	8001094 <memset>
 80001ac:	2080      	movs	r0, #128	; 0x80
 80001ae:	0080      	lsls	r0, r0, #2
 80001b0:	f000 fadc 	bl	800076c <HAL_PWREx_ControlVoltageScaling>
 80001b4:	2302      	movs	r3, #2
 80001b6:	9304      	str	r3, [sp, #16]
 80001b8:	33fe      	adds	r3, #254	; 0xfe
 80001ba:	2240      	movs	r2, #64	; 0x40
 80001bc:	9307      	str	r3, [sp, #28]
 80001be:	2300      	movs	r3, #0
 80001c0:	a804      	add	r0, sp, #16
 80001c2:	9308      	str	r3, [sp, #32]
 80001c4:	9209      	str	r2, [sp, #36]	; 0x24
 80001c6:	930b      	str	r3, [sp, #44]	; 0x2c
 80001c8:	f000 fb00 	bl	80007cc <HAL_RCC_OscConfig>
 80001cc:	2800      	cmp	r0, #0
 80001ce:	d001      	beq.n	80001d4 <SystemClock_Config+0x40>
 80001d0:	b672      	cpsid	i
 80001d2:	e7fe      	b.n	80001d2 <SystemClock_Config+0x3e>
 80001d4:	2207      	movs	r2, #7
 80001d6:	2300      	movs	r3, #0
 80001d8:	9200      	str	r2, [sp, #0]
 80001da:	9301      	str	r3, [sp, #4]
 80001dc:	2200      	movs	r2, #0
 80001de:	2300      	movs	r3, #0
 80001e0:	2100      	movs	r1, #0
 80001e2:	4668      	mov	r0, sp
 80001e4:	9202      	str	r2, [sp, #8]
 80001e6:	9303      	str	r3, [sp, #12]
 80001e8:	f000 fdae 	bl	8000d48 <HAL_RCC_ClockConfig>
 80001ec:	2800      	cmp	r0, #0
 80001ee:	d001      	beq.n	80001f4 <SystemClock_Config+0x60>
 80001f0:	b672      	cpsid	i
 80001f2:	e7fe      	b.n	80001f2 <SystemClock_Config+0x5e>
 80001f4:	b013      	add	sp, #76	; 0x4c
 80001f6:	bd00      	pop	{pc}

080001f8 <main>:
 80001f8:	b5f0      	push	{r4, r5, r6, r7, lr}
 80001fa:	2502      	movs	r5, #2
 80001fc:	b08b      	sub	sp, #44	; 0x2c
 80001fe:	f000 f959 	bl	80004b4 <HAL_Init>
 8000202:	f7ff ffc7 	bl	8000194 <SystemClock_Config>
 8000206:	2214      	movs	r2, #20
 8000208:	2100      	movs	r1, #0
 800020a:	a804      	add	r0, sp, #16
 800020c:	f000 ff42 	bl	8001094 <memset>
 8000210:	4b4b      	ldr	r3, [pc, #300]	; (8000340 <main+0x148>)
 8000212:	20a0      	movs	r0, #160	; 0xa0
 8000214:	6b5a      	ldr	r2, [r3, #52]	; 0x34
 8000216:	05c0      	lsls	r0, r0, #23
 8000218:	432a      	orrs	r2, r5
 800021a:	635a      	str	r2, [r3, #52]	; 0x34
 800021c:	6b5a      	ldr	r2, [r3, #52]	; 0x34
 800021e:	2400      	movs	r4, #0
 8000220:	402a      	ands	r2, r5
 8000222:	9201      	str	r2, [sp, #4]
 8000224:	9a01      	ldr	r2, [sp, #4]
 8000226:	2201      	movs	r2, #1
 8000228:	6b59      	ldr	r1, [r3, #52]	; 0x34
 800022a:	2600      	movs	r6, #0
 800022c:	4311      	orrs	r1, r2
 800022e:	6359      	str	r1, [r3, #52]	; 0x34
 8000230:	6b59      	ldr	r1, [r3, #52]	; 0x34
 8000232:	2700      	movs	r7, #0
 8000234:	400a      	ands	r2, r1
 8000236:	9202      	str	r2, [sp, #8]
 8000238:	9a02      	ldr	r2, [sp, #8]
 800023a:	2204      	movs	r2, #4
 800023c:	6b59      	ldr	r1, [r3, #52]	; 0x34
 800023e:	4311      	orrs	r1, r2
 8000240:	6359      	str	r1, [r3, #52]	; 0x34
 8000242:	6b5b      	ldr	r3, [r3, #52]	; 0x34
 8000244:	493f      	ldr	r1, [pc, #252]	; (8000344 <main+0x14c>)
 8000246:	401a      	ands	r2, r3
 8000248:	9203      	str	r2, [sp, #12]
 800024a:	2200      	movs	r2, #0
 800024c:	9b03      	ldr	r3, [sp, #12]
 800024e:	f000 fa87 	bl	8000760 <HAL_GPIO_WritePin>
 8000252:	2200      	movs	r2, #0
 8000254:	210b      	movs	r1, #11
 8000256:	483c      	ldr	r0, [pc, #240]	; (8000348 <main+0x150>)
 8000258:	f000 fa82 	bl	8000760 <HAL_GPIO_WritePin>
 800025c:	2200      	movs	r2, #0
 800025e:	2140      	movs	r1, #64	; 0x40
 8000260:	483a      	ldr	r0, [pc, #232]	; (800034c <main+0x154>)
 8000262:	f000 fa7d 	bl	8000760 <HAL_GPIO_WritePin>
 8000266:	22fc      	movs	r2, #252	; 0xfc
 8000268:	2300      	movs	r3, #0
 800026a:	0092      	lsls	r2, r2, #2
 800026c:	4836      	ldr	r0, [pc, #216]	; (8000348 <main+0x150>)
 800026e:	a904      	add	r1, sp, #16
 8000270:	9204      	str	r2, [sp, #16]
 8000272:	9305      	str	r3, [sp, #20]
 8000274:	9406      	str	r4, [sp, #24]
 8000276:	f000 f995 	bl	80005a4 <HAL_GPIO_Init>
 800027a:	20a0      	movs	r0, #160	; 0xa0
 800027c:	2203      	movs	r2, #3
 800027e:	2302      	movs	r3, #2
 8000280:	a904      	add	r1, sp, #16
 8000282:	05c0      	lsls	r0, r0, #23
 8000284:	9204      	str	r2, [sp, #16]
 8000286:	9305      	str	r3, [sp, #20]
 8000288:	9606      	str	r6, [sp, #24]
 800028a:	9707      	str	r7, [sp, #28]
 800028c:	9508      	str	r5, [sp, #32]
 800028e:	f000 f989 	bl	80005a4 <HAL_GPIO_Init>
 8000292:	20a0      	movs	r0, #160	; 0xa0
 8000294:	2301      	movs	r3, #1
 8000296:	4a2b      	ldr	r2, [pc, #172]	; (8000344 <main+0x14c>)
 8000298:	a904      	add	r1, sp, #16
 800029a:	05c0      	lsls	r0, r0, #23
 800029c:	9204      	str	r2, [sp, #16]
 800029e:	9305      	str	r3, [sp, #20]
 80002a0:	9606      	str	r6, [sp, #24]
 80002a2:	9707      	str	r7, [sp, #28]
 80002a4:	f000 f97e 	bl	80005a4 <HAL_GPIO_Init>
 80002a8:	20a0      	movs	r0, #160	; 0xa0
 80002aa:	2210      	movs	r2, #16
 80002ac:	2303      	movs	r3, #3
 80002ae:	a904      	add	r1, sp, #16
 80002b0:	05c0      	lsls	r0, r0, #23
 80002b2:	9204      	str	r2, [sp, #16]
 80002b4:	9305      	str	r3, [sp, #20]
 80002b6:	9406      	str	r4, [sp, #24]
 80002b8:	f000 f974 	bl	80005a4 <HAL_GPIO_Init>
 80002bc:	220b      	movs	r2, #11
 80002be:	2301      	movs	r3, #1
 80002c0:	4821      	ldr	r0, [pc, #132]	; (8000348 <main+0x150>)
 80002c2:	a904      	add	r1, sp, #16
 80002c4:	9204      	str	r2, [sp, #16]
 80002c6:	9305      	str	r3, [sp, #20]
 80002c8:	9606      	str	r6, [sp, #24]
 80002ca:	9707      	str	r7, [sp, #28]
 80002cc:	f000 f96a 	bl	80005a4 <HAL_GPIO_Init>
 80002d0:	2301      	movs	r3, #1
 80002d2:	2240      	movs	r2, #64	; 0x40
 80002d4:	481d      	ldr	r0, [pc, #116]	; (800034c <main+0x154>)
 80002d6:	a904      	add	r1, sp, #16
 80002d8:	9204      	str	r2, [sp, #16]
 80002da:	9305      	str	r3, [sp, #20]
 80002dc:	9606      	str	r6, [sp, #24]
 80002de:	9707      	str	r7, [sp, #28]
 80002e0:	f000 f960 	bl	80005a4 <HAL_GPIO_Init>
 80002e4:	481a      	ldr	r0, [pc, #104]	; (8000350 <main+0x158>)
 80002e6:	4b1b      	ldr	r3, [pc, #108]	; (8000354 <main+0x15c>)
 80002e8:	6084      	str	r4, [r0, #8]
 80002ea:	6003      	str	r3, [r0, #0]
 80002ec:	2382      	movs	r3, #130	; 0x82
 80002ee:	005b      	lsls	r3, r3, #1
 80002f0:	6043      	str	r3, [r0, #4]
 80002f2:	23e0      	movs	r3, #224	; 0xe0
 80002f4:	00db      	lsls	r3, r3, #3
 80002f6:	60c3      	str	r3, [r0, #12]
 80002f8:	2380      	movs	r3, #128	; 0x80
 80002fa:	009b      	lsls	r3, r3, #2
 80002fc:	6183      	str	r3, [r0, #24]
 80002fe:	3bfa      	subs	r3, #250	; 0xfa
 8000300:	3bff      	subs	r3, #255	; 0xff
 8000302:	6104      	str	r4, [r0, #16]
 8000304:	6144      	str	r4, [r0, #20]
 8000306:	61c4      	str	r4, [r0, #28]
 8000308:	6204      	str	r4, [r0, #32]
 800030a:	6244      	str	r4, [r0, #36]	; 0x24
 800030c:	6284      	str	r4, [r0, #40]	; 0x28
 800030e:	62c3      	str	r3, [r0, #44]	; 0x2c
 8000310:	6304      	str	r4, [r0, #48]	; 0x30
 8000312:	6344      	str	r4, [r0, #52]	; 0x34
 8000314:	f000 fde0 	bl	8000ed8 <HAL_SPI_Init>
 8000318:	2800      	cmp	r0, #0
 800031a:	d001      	beq.n	8000320 <main+0x128>
 800031c:	b672      	cpsid	i
 800031e:	e7fe      	b.n	800031e <main+0x126>
 8000320:	2180      	movs	r1, #128	; 0x80
 8000322:	20a0      	movs	r0, #160	; 0xa0
 8000324:	2201      	movs	r2, #1
 8000326:	0209      	lsls	r1, r1, #8
 8000328:	05c0      	lsls	r0, r0, #23
 800032a:	f000 fa19 	bl	8000760 <HAL_GPIO_WritePin>
 800032e:	2180      	movs	r1, #128	; 0x80
 8000330:	20a0      	movs	r0, #160	; 0xa0
 8000332:	2200      	movs	r2, #0
 8000334:	0209      	lsls	r1, r1, #8
 8000336:	05c0      	lsls	r0, r0, #23
 8000338:	f000 fa12 	bl	8000760 <HAL_GPIO_WritePin>
 800033c:	e7f0      	b.n	8000320 <main+0x128>
 800033e:	46c0      	nop			; (mov r8, r8)
 8000340:	40021000 	.word	0x40021000
 8000344:	00009f4c 	.word	0x00009f4c
 8000348:	50000400 	.word	0x50000400
 800034c:	50000800 	.word	0x50000800
 8000350:	20000458 	.word	0x20000458
 8000354:	40013000 	.word	0x40013000

08000358 <HAL_MspInit>:
 8000358:	2201      	movs	r2, #1
 800035a:	4b0c      	ldr	r3, [pc, #48]	; (800038c <HAL_MspInit+0x34>)
 800035c:	b500      	push	{lr}
 800035e:	6c19      	ldr	r1, [r3, #64]	; 0x40
 8000360:	2080      	movs	r0, #128	; 0x80
 8000362:	4311      	orrs	r1, r2
 8000364:	6419      	str	r1, [r3, #64]	; 0x40
 8000366:	6c19      	ldr	r1, [r3, #64]	; 0x40
 8000368:	b083      	sub	sp, #12
 800036a:	400a      	ands	r2, r1
 800036c:	2180      	movs	r1, #128	; 0x80
 800036e:	9200      	str	r2, [sp, #0]
 8000370:	9a00      	ldr	r2, [sp, #0]
 8000372:	6bda      	ldr	r2, [r3, #60]	; 0x3c
 8000374:	0549      	lsls	r1, r1, #21
 8000376:	430a      	orrs	r2, r1
 8000378:	63da      	str	r2, [r3, #60]	; 0x3c
 800037a:	6bdb      	ldr	r3, [r3, #60]	; 0x3c
 800037c:	0080      	lsls	r0, r0, #2
 800037e:	400b      	ands	r3, r1
 8000380:	9301      	str	r3, [sp, #4]
 8000382:	9b01      	ldr	r3, [sp, #4]
 8000384:	f000 f8bc 	bl	8000500 <HAL_SYSCFG_StrobeDBattpinsConfig>
 8000388:	b003      	add	sp, #12
 800038a:	bd00      	pop	{pc}
 800038c:	40021000 	.word	0x40021000

08000390 <HAL_SPI_MspInit>:
 8000390:	b510      	push	{r4, lr}
 8000392:	0004      	movs	r4, r0
 8000394:	b088      	sub	sp, #32
 8000396:	2214      	movs	r2, #20
 8000398:	2100      	movs	r1, #0
 800039a:	a802      	add	r0, sp, #8
 800039c:	f000 fe7a 	bl	8001094 <memset>
 80003a0:	4b10      	ldr	r3, [pc, #64]	; (80003e4 <HAL_SPI_MspInit+0x54>)
 80003a2:	6822      	ldr	r2, [r4, #0]
 80003a4:	429a      	cmp	r2, r3
 80003a6:	d001      	beq.n	80003ac <HAL_SPI_MspInit+0x1c>
 80003a8:	b008      	add	sp, #32
 80003aa:	bd10      	pop	{r4, pc}
 80003ac:	2080      	movs	r0, #128	; 0x80
 80003ae:	4b0e      	ldr	r3, [pc, #56]	; (80003e8 <HAL_SPI_MspInit+0x58>)
 80003b0:	0140      	lsls	r0, r0, #5
 80003b2:	6c19      	ldr	r1, [r3, #64]	; 0x40
 80003b4:	4301      	orrs	r1, r0
 80003b6:	6419      	str	r1, [r3, #64]	; 0x40
 80003b8:	6c1a      	ldr	r2, [r3, #64]	; 0x40
 80003ba:	4002      	ands	r2, r0
 80003bc:	9200      	str	r2, [sp, #0]
 80003be:	9a00      	ldr	r2, [sp, #0]
 80003c0:	2201      	movs	r2, #1
 80003c2:	6b59      	ldr	r1, [r3, #52]	; 0x34
 80003c4:	20a0      	movs	r0, #160	; 0xa0
 80003c6:	4311      	orrs	r1, r2
 80003c8:	6359      	str	r1, [r3, #52]	; 0x34
 80003ca:	6b5b      	ldr	r3, [r3, #52]	; 0x34
 80003cc:	a902      	add	r1, sp, #8
 80003ce:	401a      	ands	r2, r3
 80003d0:	9201      	str	r2, [sp, #4]
 80003d2:	9b01      	ldr	r3, [sp, #4]
 80003d4:	22a0      	movs	r2, #160	; 0xa0
 80003d6:	2302      	movs	r3, #2
 80003d8:	05c0      	lsls	r0, r0, #23
 80003da:	9202      	str	r2, [sp, #8]
 80003dc:	9303      	str	r3, [sp, #12]
 80003de:	f000 f8e1 	bl	80005a4 <HAL_GPIO_Init>
 80003e2:	e7e1      	b.n	80003a8 <HAL_SPI_MspInit+0x18>
 80003e4:	40013000 	.word	0x40013000
 80003e8:	40021000 	.word	0x40021000

080003ec <NMI_Handler>:
 80003ec:	e7fe      	b.n	80003ec <NMI_Handler>
 80003ee:	46c0      	nop			; (mov r8, r8)

080003f0 <HardFault_Handler>:
 80003f0:	e7fe      	b.n	80003f0 <HardFault_Handler>
 80003f2:	46c0      	nop			; (mov r8, r8)

080003f4 <SVC_Handler>:
 80003f4:	4770      	bx	lr
 80003f6:	46c0      	nop			; (mov r8, r8)

080003f8 <PendSV_Handler>:
 80003f8:	4770      	bx	lr
 80003fa:	46c0      	nop			; (mov r8, r8)

080003fc <SysTick_Handler>:
 80003fc:	b510      	push	{r4, lr}
 80003fe:	f000 f86d 	bl	80004dc <HAL_IncTick>
 8000402:	bd10      	pop	{r4, pc}

08000404 <SystemInit>:
 8000404:	2280      	movs	r2, #128	; 0x80
 8000406:	4b02      	ldr	r3, [pc, #8]	; (8000410 <SystemInit+0xc>)
 8000408:	0512      	lsls	r2, r2, #20
 800040a:	609a      	str	r2, [r3, #8]
 800040c:	4770      	bx	lr
 800040e:	46c0      	nop			; (mov r8, r8)
 8000410:	e000ed00 	.word	0xe000ed00

08000414 <Reset_Handler>:
 8000414:	480d      	ldr	r0, [pc, #52]	; (800044c <LoopForever+0x2>)
 8000416:	4685      	mov	sp, r0
 8000418:	f7ff fff4 	bl	8000404 <SystemInit>
 800041c:	480c      	ldr	r0, [pc, #48]	; (8000450 <LoopForever+0x6>)
 800041e:	490d      	ldr	r1, [pc, #52]	; (8000454 <LoopForever+0xa>)
 8000420:	4a0d      	ldr	r2, [pc, #52]	; (8000458 <LoopForever+0xe>)
 8000422:	2300      	movs	r3, #0
 8000424:	e002      	b.n	800042c <LoopCopyDataInit>

08000426 <CopyDataInit>:
 8000426:	58d4      	ldr	r4, [r2, r3]
 8000428:	50c4      	str	r4, [r0, r3]
 800042a:	3304      	adds	r3, #4

0800042c <LoopCopyDataInit>:
 800042c:	18c4      	adds	r4, r0, r3
 800042e:	428c      	cmp	r4, r1
 8000430:	d3f9      	bcc.n	8000426 <CopyDataInit>
 8000432:	4a0a      	ldr	r2, [pc, #40]	; (800045c <LoopForever+0x12>)
 8000434:	4c0a      	ldr	r4, [pc, #40]	; (8000460 <LoopForever+0x16>)
 8000436:	2300      	movs	r3, #0
 8000438:	e001      	b.n	800043e <LoopFillZerobss>

0800043a <FillZerobss>:
 800043a:	6013      	str	r3, [r2, #0]
 800043c:	3204      	adds	r2, #4

0800043e <LoopFillZerobss>:
 800043e:	42a2      	cmp	r2, r4
 8000440:	d3fb      	bcc.n	800043a <FillZerobss>
 8000442:	f000 fe05 	bl	8001050 <__libc_init_array>
 8000446:	f7ff fed7 	bl	80001f8 <main>

0800044a <LoopForever>:
 800044a:	e7fe      	b.n	800044a <LoopForever>
 800044c:	20009000 	.word	0x20009000
 8000450:	20000000 	.word	0x20000000
 8000454:	2000043c 	.word	0x2000043c
 8000458:	08001274 	.word	0x08001274
 800045c:	2000043c 	.word	0x2000043c
 8000460:	200004c4 	.word	0x200004c4

08000464 <ADC1_COMP_IRQHandler>:
 8000464:	e7fe      	b.n	8000464 <ADC1_COMP_IRQHandler>
	...

08000468 <HAL_InitTick>:
 8000468:	b510      	push	{r4, lr}
 800046a:	4b0f      	ldr	r3, [pc, #60]	; (80004a8 <HAL_InitTick+0x40>)
 800046c:	0004      	movs	r4, r0
 800046e:	7819      	ldrb	r1, [r3, #0]
 8000470:	2900      	cmp	r1, #0
 8000472:	d101      	bne.n	8000478 <HAL_InitTick+0x10>
 8000474:	2001      	movs	r0, #1
 8000476:	bd10      	pop	{r4, pc}
 8000478:	20fa      	movs	r0, #250	; 0xfa
 800047a:	0080      	lsls	r0, r0, #2
 800047c:	f7ff fe1e 	bl	80000bc <__udivsi3>
 8000480:	4b0a      	ldr	r3, [pc, #40]	; (80004ac <HAL_InitTick+0x44>)
 8000482:	0001      	movs	r1, r0
 8000484:	6818      	ldr	r0, [r3, #0]
 8000486:	f7ff fe19 	bl	80000bc <__udivsi3>
 800048a:	f000 f871 	bl	8000570 <HAL_SYSTICK_Config>
 800048e:	2800      	cmp	r0, #0
 8000490:	d1f0      	bne.n	8000474 <HAL_InitTick+0xc>
 8000492:	2c03      	cmp	r4, #3
 8000494:	d8ee      	bhi.n	8000474 <HAL_InitTick+0xc>
 8000496:	3801      	subs	r0, #1
 8000498:	2200      	movs	r2, #0
 800049a:	0021      	movs	r1, r4
 800049c:	f000 f83c 	bl	8000518 <HAL_NVIC_SetPriority>
 80004a0:	4b03      	ldr	r3, [pc, #12]	; (80004b0 <HAL_InitTick+0x48>)
 80004a2:	2000      	movs	r0, #0
 80004a4:	601c      	str	r4, [r3, #0]
 80004a6:	e7e6      	b.n	8000476 <HAL_InitTick+0xe>
 80004a8:	20000004 	.word	0x20000004
 80004ac:	20000000 	.word	0x20000000
 80004b0:	20000008 	.word	0x20000008

080004b4 <HAL_Init>:
 80004b4:	2380      	movs	r3, #128	; 0x80
 80004b6:	4a08      	ldr	r2, [pc, #32]	; (80004d8 <HAL_Init+0x24>)
 80004b8:	005b      	lsls	r3, r3, #1
 80004ba:	6811      	ldr	r1, [r2, #0]
 80004bc:	b510      	push	{r4, lr}
 80004be:	430b      	orrs	r3, r1
 80004c0:	2000      	movs	r0, #0
 80004c2:	6013      	str	r3, [r2, #0]
 80004c4:	f7ff ffd0 	bl	8000468 <HAL_InitTick>
 80004c8:	1e04      	subs	r4, r0, #0
 80004ca:	d002      	beq.n	80004d2 <HAL_Init+0x1e>
 80004cc:	2401      	movs	r4, #1
 80004ce:	0020      	movs	r0, r4
 80004d0:	bd10      	pop	{r4, pc}
 80004d2:	f7ff ff41 	bl	8000358 <HAL_MspInit>
 80004d6:	e7fa      	b.n	80004ce <HAL_Init+0x1a>
 80004d8:	40022000 	.word	0x40022000

080004dc <HAL_IncTick>:
 80004dc:	4a03      	ldr	r2, [pc, #12]	; (80004ec <HAL_IncTick+0x10>)
 80004de:	4b04      	ldr	r3, [pc, #16]	; (80004f0 <HAL_IncTick+0x14>)
 80004e0:	6811      	ldr	r1, [r2, #0]
 80004e2:	781b      	ldrb	r3, [r3, #0]
 80004e4:	185b      	adds	r3, r3, r1
 80004e6:	6013      	str	r3, [r2, #0]
 80004e8:	4770      	bx	lr
 80004ea:	46c0      	nop			; (mov r8, r8)
 80004ec:	200004bc 	.word	0x200004bc
 80004f0:	20000004 	.word	0x20000004

080004f4 <HAL_GetTick>:
 80004f4:	4b01      	ldr	r3, [pc, #4]	; (80004fc <HAL_GetTick+0x8>)
 80004f6:	6818      	ldr	r0, [r3, #0]
 80004f8:	4770      	bx	lr
 80004fa:	46c0      	nop			; (mov r8, r8)
 80004fc:	200004bc 	.word	0x200004bc

08000500 <HAL_SYSCFG_StrobeDBattpinsConfig>:
 8000500:	4a03      	ldr	r2, [pc, #12]	; (8000510 <HAL_SYSCFG_StrobeDBattpinsConfig+0x10>)
 8000502:	4904      	ldr	r1, [pc, #16]	; (8000514 <HAL_SYSCFG_StrobeDBattpinsConfig+0x14>)
 8000504:	6813      	ldr	r3, [r2, #0]
 8000506:	400b      	ands	r3, r1
 8000508:	4303      	orrs	r3, r0
 800050a:	6013      	str	r3, [r2, #0]
 800050c:	4770      	bx	lr
 800050e:	46c0      	nop			; (mov r8, r8)
 8000510:	40010000 	.word	0x40010000
 8000514:	fffff9ff 	.word	0xfffff9ff

08000518 <HAL_NVIC_SetPriority>:
 8000518:	22ff      	movs	r2, #255	; 0xff
 800051a:	2303      	movs	r3, #3
 800051c:	b530      	push	{r4, r5, lr}
 800051e:	0014      	movs	r4, r2
 8000520:	4003      	ands	r3, r0
 8000522:	00db      	lsls	r3, r3, #3
 8000524:	409c      	lsls	r4, r3
 8000526:	0189      	lsls	r1, r1, #6
 8000528:	400a      	ands	r2, r1
 800052a:	43e4      	mvns	r4, r4
 800052c:	409a      	lsls	r2, r3
 800052e:	b2c5      	uxtb	r5, r0
 8000530:	2800      	cmp	r0, #0
 8000532:	db0b      	blt.n	800054c <HAL_NVIC_SetPriority+0x34>
 8000534:	4b0c      	ldr	r3, [pc, #48]	; (8000568 <HAL_NVIC_SetPriority+0x50>)
 8000536:	0880      	lsrs	r0, r0, #2
 8000538:	469c      	mov	ip, r3
 800053a:	23c0      	movs	r3, #192	; 0xc0
 800053c:	0080      	lsls	r0, r0, #2
 800053e:	4460      	add	r0, ip
 8000540:	009b      	lsls	r3, r3, #2
 8000542:	58c1      	ldr	r1, [r0, r3]
 8000544:	400c      	ands	r4, r1
 8000546:	4322      	orrs	r2, r4
 8000548:	50c2      	str	r2, [r0, r3]
 800054a:	bd30      	pop	{r4, r5, pc}
 800054c:	230f      	movs	r3, #15
 800054e:	4907      	ldr	r1, [pc, #28]	; (800056c <HAL_NVIC_SetPriority+0x54>)
 8000550:	402b      	ands	r3, r5
 8000552:	468c      	mov	ip, r1
 8000554:	3b08      	subs	r3, #8
 8000556:	089b      	lsrs	r3, r3, #2
 8000558:	009b      	lsls	r3, r3, #2
 800055a:	4463      	add	r3, ip
 800055c:	69d9      	ldr	r1, [r3, #28]
 800055e:	400c      	ands	r4, r1
 8000560:	4322      	orrs	r2, r4
 8000562:	61da      	str	r2, [r3, #28]
 8000564:	e7f1      	b.n	800054a <HAL_NVIC_SetPriority+0x32>
 8000566:	46c0      	nop			; (mov r8, r8)
 8000568:	e000e100 	.word	0xe000e100
 800056c:	e000ed00 	.word	0xe000ed00

08000570 <HAL_SYSTICK_Config>:
 8000570:	2280      	movs	r2, #128	; 0x80
 8000572:	1e43      	subs	r3, r0, #1
 8000574:	0452      	lsls	r2, r2, #17
 8000576:	2001      	movs	r0, #1
 8000578:	4293      	cmp	r3, r2
 800057a:	d20e      	bcs.n	800059a <HAL_SYSTICK_Config+0x2a>
 800057c:	21c0      	movs	r1, #192	; 0xc0
 800057e:	4a07      	ldr	r2, [pc, #28]	; (800059c <HAL_SYSTICK_Config+0x2c>)
 8000580:	4807      	ldr	r0, [pc, #28]	; (80005a0 <HAL_SYSTICK_Config+0x30>)
 8000582:	6053      	str	r3, [r2, #4]
 8000584:	6a03      	ldr	r3, [r0, #32]
 8000586:	0609      	lsls	r1, r1, #24
 8000588:	021b      	lsls	r3, r3, #8
 800058a:	0a1b      	lsrs	r3, r3, #8
 800058c:	430b      	orrs	r3, r1
 800058e:	6203      	str	r3, [r0, #32]
 8000590:	2300      	movs	r3, #0
 8000592:	2000      	movs	r0, #0
 8000594:	6093      	str	r3, [r2, #8]
 8000596:	3307      	adds	r3, #7
 8000598:	6013      	str	r3, [r2, #0]
 800059a:	4770      	bx	lr
 800059c:	e000e010 	.word	0xe000e010
 80005a0:	e000ed00 	.word	0xe000ed00

080005a4 <HAL_GPIO_Init>:
 80005a4:	b5f0      	push	{r4, r5, r6, r7, lr}
 80005a6:	4645      	mov	r5, r8
 80005a8:	46de      	mov	lr, fp
 80005aa:	4657      	mov	r7, sl
 80005ac:	464e      	mov	r6, r9
 80005ae:	b5e0      	push	{r5, r6, r7, lr}
 80005b0:	680d      	ldr	r5, [r1, #0]
 80005b2:	4688      	mov	r8, r1
 80005b4:	2300      	movs	r3, #0
 80005b6:	b083      	sub	sp, #12
 80005b8:	2d00      	cmp	r5, #0
 80005ba:	d071      	beq.n	80006a0 <HAL_GPIO_Init+0xfc>
 80005bc:	2201      	movs	r2, #1
 80005be:	9200      	str	r2, [sp, #0]
 80005c0:	4a63      	ldr	r2, [pc, #396]	; (8000750 <HAL_GPIO_Init+0x1ac>)
 80005c2:	46ac      	mov	ip, r5
 80005c4:	4692      	mov	sl, r2
 80005c6:	9900      	ldr	r1, [sp, #0]
 80005c8:	4662      	mov	r2, ip
 80005ca:	4099      	lsls	r1, r3
 80005cc:	400a      	ands	r2, r1
 80005ce:	4691      	mov	r9, r2
 80005d0:	4662      	mov	r2, ip
 80005d2:	420a      	tst	r2, r1
 80005d4:	d060      	beq.n	8000698 <HAL_GPIO_Init+0xf4>
 80005d6:	4642      	mov	r2, r8
 80005d8:	6854      	ldr	r4, [r2, #4]
 80005da:	2210      	movs	r2, #16
 80005dc:	0026      	movs	r6, r4
 80005de:	005d      	lsls	r5, r3, #1
 80005e0:	4396      	bics	r6, r2
 80005e2:	3a0d      	subs	r2, #13
 80005e4:	40aa      	lsls	r2, r5
 80005e6:	43d2      	mvns	r2, r2
 80005e8:	4693      	mov	fp, r2
 80005ea:	4642      	mov	r2, r8
 80005ec:	6897      	ldr	r7, [r2, #8]
 80005ee:	9601      	str	r6, [sp, #4]
 80005f0:	3e01      	subs	r6, #1
 80005f2:	40af      	lsls	r7, r5
 80005f4:	2e01      	cmp	r6, #1
 80005f6:	d95a      	bls.n	80006ae <HAL_GPIO_Init+0x10a>
 80005f8:	465a      	mov	r2, fp
 80005fa:	68c1      	ldr	r1, [r0, #12]
 80005fc:	4011      	ands	r1, r2
 80005fe:	430f      	orrs	r7, r1
 8000600:	60c7      	str	r7, [r0, #12]
 8000602:	465a      	mov	r2, fp
 8000604:	6801      	ldr	r1, [r0, #0]
 8000606:	400a      	ands	r2, r1
 8000608:	2103      	movs	r1, #3
 800060a:	4021      	ands	r1, r4
 800060c:	40a9      	lsls	r1, r5
 800060e:	430a      	orrs	r2, r1
 8000610:	6002      	str	r2, [r0, #0]
 8000612:	2280      	movs	r2, #128	; 0x80
 8000614:	0552      	lsls	r2, r2, #21
 8000616:	4214      	tst	r4, r2
 8000618:	d03e      	beq.n	8000698 <HAL_GPIO_Init+0xf4>
 800061a:	2503      	movs	r5, #3
 800061c:	260f      	movs	r6, #15
 800061e:	494c      	ldr	r1, [pc, #304]	; (8000750 <HAL_GPIO_Init+0x1ac>)
 8000620:	401d      	ands	r5, r3
 8000622:	468b      	mov	fp, r1
 8000624:	00ed      	lsls	r5, r5, #3
 8000626:	40ae      	lsls	r6, r5
 8000628:	089a      	lsrs	r2, r3, #2
 800062a:	0092      	lsls	r2, r2, #2
 800062c:	445a      	add	r2, fp
 800062e:	6e11      	ldr	r1, [r2, #96]	; 0x60
 8000630:	43b1      	bics	r1, r6
 8000632:	26a0      	movs	r6, #160	; 0xa0
 8000634:	05f6      	lsls	r6, r6, #23
 8000636:	42b0      	cmp	r0, r6
 8000638:	d00e      	beq.n	8000658 <HAL_GPIO_Init+0xb4>
 800063a:	4e46      	ldr	r6, [pc, #280]	; (8000754 <HAL_GPIO_Init+0x1b0>)
 800063c:	42b0      	cmp	r0, r6
 800063e:	d100      	bne.n	8000642 <HAL_GPIO_Init+0x9e>
 8000640:	e07d      	b.n	800073e <HAL_GPIO_Init+0x19a>
 8000642:	4e45      	ldr	r6, [pc, #276]	; (8000758 <HAL_GPIO_Init+0x1b4>)
 8000644:	42b0      	cmp	r0, r6
 8000646:	d100      	bne.n	800064a <HAL_GPIO_Init+0xa6>
 8000648:	e07d      	b.n	8000746 <HAL_GPIO_Init+0x1a2>
 800064a:	4e44      	ldr	r6, [pc, #272]	; (800075c <HAL_GPIO_Init+0x1b8>)
 800064c:	42b0      	cmp	r0, r6
 800064e:	d100      	bne.n	8000652 <HAL_GPIO_Init+0xae>
 8000650:	e071      	b.n	8000736 <HAL_GPIO_Init+0x192>
 8000652:	2605      	movs	r6, #5
 8000654:	40ae      	lsls	r6, r5
 8000656:	4331      	orrs	r1, r6
 8000658:	6611      	str	r1, [r2, #96]	; 0x60
 800065a:	2180      	movs	r1, #128	; 0x80
 800065c:	4652      	mov	r2, sl
 800065e:	5852      	ldr	r2, [r2, r1]
 8000660:	4649      	mov	r1, r9
 8000662:	43cd      	mvns	r5, r1
 8000664:	03e1      	lsls	r1, r4, #15
 8000666:	d44a      	bmi.n	80006fe <HAL_GPIO_Init+0x15a>
 8000668:	4651      	mov	r1, sl
 800066a:	2680      	movs	r6, #128	; 0x80
 800066c:	402a      	ands	r2, r5
 800066e:	518a      	str	r2, [r1, r6]
 8000670:	2284      	movs	r2, #132	; 0x84
 8000672:	5889      	ldr	r1, [r1, r2]
 8000674:	03a2      	lsls	r2, r4, #14
 8000676:	d44b      	bmi.n	8000710 <HAL_GPIO_Init+0x16c>
 8000678:	4656      	mov	r6, sl
 800067a:	2284      	movs	r2, #132	; 0x84
 800067c:	4029      	ands	r1, r5
 800067e:	50b1      	str	r1, [r6, r2]
 8000680:	6831      	ldr	r1, [r6, #0]
 8000682:	02e2      	lsls	r2, r4, #11
 8000684:	d44c      	bmi.n	8000720 <HAL_GPIO_Init+0x17c>
 8000686:	4652      	mov	r2, sl
 8000688:	4029      	ands	r1, r5
 800068a:	6011      	str	r1, [r2, #0]
 800068c:	6852      	ldr	r2, [r2, #4]
 800068e:	02a1      	lsls	r1, r4, #10
 8000690:	d44d      	bmi.n	800072e <HAL_GPIO_Init+0x18a>
 8000692:	402a      	ands	r2, r5
 8000694:	4651      	mov	r1, sl
 8000696:	604a      	str	r2, [r1, #4]
 8000698:	4662      	mov	r2, ip
 800069a:	3301      	adds	r3, #1
 800069c:	40da      	lsrs	r2, r3
 800069e:	d192      	bne.n	80005c6 <HAL_GPIO_Init+0x22>
 80006a0:	b003      	add	sp, #12
 80006a2:	bcf0      	pop	{r4, r5, r6, r7}
 80006a4:	46bb      	mov	fp, r7
 80006a6:	46b2      	mov	sl, r6
 80006a8:	46a9      	mov	r9, r5
 80006aa:	46a0      	mov	r8, r4
 80006ac:	bdf0      	pop	{r4, r5, r6, r7, pc}
 80006ae:	465a      	mov	r2, fp
 80006b0:	6886      	ldr	r6, [r0, #8]
 80006b2:	4016      	ands	r6, r2
 80006b4:	4642      	mov	r2, r8
 80006b6:	68d2      	ldr	r2, [r2, #12]
 80006b8:	40aa      	lsls	r2, r5
 80006ba:	4316      	orrs	r6, r2
 80006bc:	6086      	str	r6, [r0, #8]
 80006be:	6846      	ldr	r6, [r0, #4]
 80006c0:	9a00      	ldr	r2, [sp, #0]
 80006c2:	438e      	bics	r6, r1
 80006c4:	0921      	lsrs	r1, r4, #4
 80006c6:	4011      	ands	r1, r2
 80006c8:	4099      	lsls	r1, r3
 80006ca:	465a      	mov	r2, fp
 80006cc:	4331      	orrs	r1, r6
 80006ce:	6041      	str	r1, [r0, #4]
 80006d0:	68c1      	ldr	r1, [r0, #12]
 80006d2:	4011      	ands	r1, r2
 80006d4:	9a01      	ldr	r2, [sp, #4]
 80006d6:	430f      	orrs	r7, r1
 80006d8:	60c7      	str	r7, [r0, #12]
 80006da:	2a02      	cmp	r2, #2
 80006dc:	d191      	bne.n	8000602 <HAL_GPIO_Init+0x5e>
 80006de:	2607      	movs	r6, #7
 80006e0:	401e      	ands	r6, r3
 80006e2:	00b6      	lsls	r6, r6, #2
 80006e4:	320d      	adds	r2, #13
 80006e6:	40b2      	lsls	r2, r6
 80006e8:	08d9      	lsrs	r1, r3, #3
 80006ea:	0089      	lsls	r1, r1, #2
 80006ec:	1841      	adds	r1, r0, r1
 80006ee:	6a0f      	ldr	r7, [r1, #32]
 80006f0:	4397      	bics	r7, r2
 80006f2:	4642      	mov	r2, r8
 80006f4:	6912      	ldr	r2, [r2, #16]
 80006f6:	40b2      	lsls	r2, r6
 80006f8:	4317      	orrs	r7, r2
 80006fa:	620f      	str	r7, [r1, #32]
 80006fc:	e781      	b.n	8000602 <HAL_GPIO_Init+0x5e>
 80006fe:	4649      	mov	r1, r9
 8000700:	2680      	movs	r6, #128	; 0x80
 8000702:	430a      	orrs	r2, r1
 8000704:	4651      	mov	r1, sl
 8000706:	518a      	str	r2, [r1, r6]
 8000708:	2284      	movs	r2, #132	; 0x84
 800070a:	5889      	ldr	r1, [r1, r2]
 800070c:	03a2      	lsls	r2, r4, #14
 800070e:	d5b3      	bpl.n	8000678 <HAL_GPIO_Init+0xd4>
 8000710:	464a      	mov	r2, r9
 8000712:	4656      	mov	r6, sl
 8000714:	4311      	orrs	r1, r2
 8000716:	2284      	movs	r2, #132	; 0x84
 8000718:	50b1      	str	r1, [r6, r2]
 800071a:	6831      	ldr	r1, [r6, #0]
 800071c:	02e2      	lsls	r2, r4, #11
 800071e:	d5b2      	bpl.n	8000686 <HAL_GPIO_Init+0xe2>
 8000720:	464a      	mov	r2, r9
 8000722:	4311      	orrs	r1, r2
 8000724:	4652      	mov	r2, sl
 8000726:	6011      	str	r1, [r2, #0]
 8000728:	6852      	ldr	r2, [r2, #4]
 800072a:	02a1      	lsls	r1, r4, #10
 800072c:	d5b1      	bpl.n	8000692 <HAL_GPIO_Init+0xee>
 800072e:	4649      	mov	r1, r9
 8000730:	4311      	orrs	r1, r2
 8000732:	000a      	movs	r2, r1
 8000734:	e7ae      	b.n	8000694 <HAL_GPIO_Init+0xf0>
 8000736:	2603      	movs	r6, #3
 8000738:	40ae      	lsls	r6, r5
 800073a:	4331      	orrs	r1, r6
 800073c:	e78c      	b.n	8000658 <HAL_GPIO_Init+0xb4>
 800073e:	9e00      	ldr	r6, [sp, #0]
 8000740:	40ae      	lsls	r6, r5
 8000742:	4331      	orrs	r1, r6
 8000744:	e788      	b.n	8000658 <HAL_GPIO_Init+0xb4>
 8000746:	2602      	movs	r6, #2
 8000748:	40ae      	lsls	r6, r5
 800074a:	4331      	orrs	r1, r6
 800074c:	e784      	b.n	8000658 <HAL_GPIO_Init+0xb4>
 800074e:	46c0      	nop			; (mov r8, r8)
 8000750:	40021800 	.word	0x40021800
 8000754:	50000400 	.word	0x50000400
 8000758:	50000800 	.word	0x50000800
 800075c:	50000c00 	.word	0x50000c00

08000760 <HAL_GPIO_WritePin>:
 8000760:	2a00      	cmp	r2, #0
 8000762:	d001      	beq.n	8000768 <HAL_GPIO_WritePin+0x8>
 8000764:	6181      	str	r1, [r0, #24]
 8000766:	4770      	bx	lr
 8000768:	6281      	str	r1, [r0, #40]	; 0x28
 800076a:	e7fc      	b.n	8000766 <HAL_GPIO_WritePin+0x6>

0800076c <HAL_PWREx_ControlVoltageScaling>:
 800076c:	b570      	push	{r4, r5, r6, lr}
 800076e:	4c13      	ldr	r4, [pc, #76]	; (80007bc <HAL_PWREx_ControlVoltageScaling+0x50>)
 8000770:	4a13      	ldr	r2, [pc, #76]	; (80007c0 <HAL_PWREx_ControlVoltageScaling+0x54>)
 8000772:	6823      	ldr	r3, [r4, #0]
 8000774:	2500      	movs	r5, #0
 8000776:	4013      	ands	r3, r2
 8000778:	4303      	orrs	r3, r0
 800077a:	6023      	str	r3, [r4, #0]
 800077c:	2380      	movs	r3, #128	; 0x80
 800077e:	009b      	lsls	r3, r3, #2
 8000780:	4298      	cmp	r0, r3
 8000782:	d001      	beq.n	8000788 <HAL_PWREx_ControlVoltageScaling+0x1c>
 8000784:	0028      	movs	r0, r5
 8000786:	bd70      	pop	{r4, r5, r6, pc}
 8000788:	4b0e      	ldr	r3, [pc, #56]	; (80007c4 <HAL_PWREx_ControlVoltageScaling+0x58>)
 800078a:	490f      	ldr	r1, [pc, #60]	; (80007c8 <HAL_PWREx_ControlVoltageScaling+0x5c>)
 800078c:	681b      	ldr	r3, [r3, #0]
 800078e:	0058      	lsls	r0, r3, #1
 8000790:	18c0      	adds	r0, r0, r3
 8000792:	0040      	lsls	r0, r0, #1
 8000794:	f7ff fc92 	bl	80000bc <__udivsi3>
 8000798:	2280      	movs	r2, #128	; 0x80
 800079a:	6963      	ldr	r3, [r4, #20]
 800079c:	00d2      	lsls	r2, r2, #3
 800079e:	3001      	adds	r0, #1
 80007a0:	4213      	tst	r3, r2
 80007a2:	d102      	bne.n	80007aa <HAL_PWREx_ControlVoltageScaling+0x3e>
 80007a4:	e7ee      	b.n	8000784 <HAL_PWREx_ControlVoltageScaling+0x18>
 80007a6:	2800      	cmp	r0, #0
 80007a8:	d005      	beq.n	80007b6 <HAL_PWREx_ControlVoltageScaling+0x4a>
 80007aa:	6963      	ldr	r3, [r4, #20]
 80007ac:	3801      	subs	r0, #1
 80007ae:	4213      	tst	r3, r2
 80007b0:	d1f9      	bne.n	80007a6 <HAL_PWREx_ControlVoltageScaling+0x3a>
 80007b2:	2500      	movs	r5, #0
 80007b4:	e7e6      	b.n	8000784 <HAL_PWREx_ControlVoltageScaling+0x18>
 80007b6:	2503      	movs	r5, #3
 80007b8:	e7e4      	b.n	8000784 <HAL_PWREx_ControlVoltageScaling+0x18>
 80007ba:	46c0      	nop			; (mov r8, r8)
 80007bc:	40007000 	.word	0x40007000
 80007c0:	fffff9ff 	.word	0xfffff9ff
 80007c4:	20000000 	.word	0x20000000
 80007c8:	000f4240 	.word	0x000f4240

080007cc <HAL_RCC_OscConfig>:
 80007cc:	b5f0      	push	{r4, r5, r6, r7, lr}
 80007ce:	46ce      	mov	lr, r9
 80007d0:	4647      	mov	r7, r8
 80007d2:	b580      	push	{r7, lr}
 80007d4:	0004      	movs	r4, r0
 80007d6:	b083      	sub	sp, #12
 80007d8:	2800      	cmp	r0, #0
 80007da:	d100      	bne.n	80007de <HAL_RCC_OscConfig+0x12>
 80007dc:	e0f9      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 80007de:	6803      	ldr	r3, [r0, #0]
 80007e0:	07da      	lsls	r2, r3, #31
 80007e2:	d531      	bpl.n	8000848 <HAL_RCC_OscConfig+0x7c>
 80007e4:	2238      	movs	r2, #56	; 0x38
 80007e6:	49c5      	ldr	r1, [pc, #788]	; (8000afc <HAL_RCC_OscConfig+0x330>)
 80007e8:	6888      	ldr	r0, [r1, #8]
 80007ea:	68c9      	ldr	r1, [r1, #12]
 80007ec:	4002      	ands	r2, r0
 80007ee:	2a10      	cmp	r2, #16
 80007f0:	d100      	bne.n	80007f4 <HAL_RCC_OscConfig+0x28>
 80007f2:	e0f4      	b.n	80009de <HAL_RCC_OscConfig+0x212>
 80007f4:	2a08      	cmp	r2, #8
 80007f6:	d100      	bne.n	80007fa <HAL_RCC_OscConfig+0x2e>
 80007f8:	e0f5      	b.n	80009e6 <HAL_RCC_OscConfig+0x21a>
 80007fa:	2280      	movs	r2, #128	; 0x80
 80007fc:	6863      	ldr	r3, [r4, #4]
 80007fe:	0252      	lsls	r2, r2, #9
 8000800:	4293      	cmp	r3, r2
 8000802:	d100      	bne.n	8000806 <HAL_RCC_OscConfig+0x3a>
 8000804:	e102      	b.n	8000a0c <HAL_RCC_OscConfig+0x240>
 8000806:	21a0      	movs	r1, #160	; 0xa0
 8000808:	02c9      	lsls	r1, r1, #11
 800080a:	428b      	cmp	r3, r1
 800080c:	d100      	bne.n	8000810 <HAL_RCC_OscConfig+0x44>
 800080e:	e1dd      	b.n	8000bcc <HAL_RCC_OscConfig+0x400>
 8000810:	4dba      	ldr	r5, [pc, #744]	; (8000afc <HAL_RCC_OscConfig+0x330>)
 8000812:	49bb      	ldr	r1, [pc, #748]	; (8000b00 <HAL_RCC_OscConfig+0x334>)
 8000814:	682a      	ldr	r2, [r5, #0]
 8000816:	400a      	ands	r2, r1
 8000818:	602a      	str	r2, [r5, #0]
 800081a:	682a      	ldr	r2, [r5, #0]
 800081c:	49b9      	ldr	r1, [pc, #740]	; (8000b04 <HAL_RCC_OscConfig+0x338>)
 800081e:	400a      	ands	r2, r1
 8000820:	602a      	str	r2, [r5, #0]
 8000822:	2b00      	cmp	r3, #0
 8000824:	d000      	beq.n	8000828 <HAL_RCC_OscConfig+0x5c>
 8000826:	e0f5      	b.n	8000a14 <HAL_RCC_OscConfig+0x248>
 8000828:	f7ff fe64 	bl	80004f4 <HAL_GetTick>
 800082c:	2780      	movs	r7, #128	; 0x80
 800082e:	0006      	movs	r6, r0
 8000830:	02bf      	lsls	r7, r7, #10
 8000832:	e005      	b.n	8000840 <HAL_RCC_OscConfig+0x74>
 8000834:	f7ff fe5e 	bl	80004f4 <HAL_GetTick>
 8000838:	1b80      	subs	r0, r0, r6
 800083a:	2864      	cmp	r0, #100	; 0x64
 800083c:	d900      	bls.n	8000840 <HAL_RCC_OscConfig+0x74>
 800083e:	e13d      	b.n	8000abc <HAL_RCC_OscConfig+0x2f0>
 8000840:	682b      	ldr	r3, [r5, #0]
 8000842:	423b      	tst	r3, r7
 8000844:	d1f6      	bne.n	8000834 <HAL_RCC_OscConfig+0x68>
 8000846:	6823      	ldr	r3, [r4, #0]
 8000848:	079a      	lsls	r2, r3, #30
 800084a:	d52c      	bpl.n	80008a6 <HAL_RCC_OscConfig+0xda>
 800084c:	2338      	movs	r3, #56	; 0x38
 800084e:	4aab      	ldr	r2, [pc, #684]	; (8000afc <HAL_RCC_OscConfig+0x330>)
 8000850:	6891      	ldr	r1, [r2, #8]
 8000852:	400b      	ands	r3, r1
 8000854:	68d1      	ldr	r1, [r2, #12]
 8000856:	2b10      	cmp	r3, #16
 8000858:	d100      	bne.n	800085c <HAL_RCC_OscConfig+0x90>
 800085a:	e0ec      	b.n	8000a36 <HAL_RCC_OscConfig+0x26a>
 800085c:	2b00      	cmp	r3, #0
 800085e:	d000      	beq.n	8000862 <HAL_RCC_OscConfig+0x96>
 8000860:	e0ee      	b.n	8000a40 <HAL_RCC_OscConfig+0x274>
 8000862:	6813      	ldr	r3, [r2, #0]
 8000864:	055b      	lsls	r3, r3, #21
 8000866:	d500      	bpl.n	800086a <HAL_RCC_OscConfig+0x9e>
 8000868:	e0af      	b.n	80009ca <HAL_RCC_OscConfig+0x1fe>
 800086a:	6851      	ldr	r1, [r2, #4]
 800086c:	6963      	ldr	r3, [r4, #20]
 800086e:	48a6      	ldr	r0, [pc, #664]	; (8000b08 <HAL_RCC_OscConfig+0x33c>)
 8000870:	021b      	lsls	r3, r3, #8
 8000872:	4001      	ands	r1, r0
 8000874:	430b      	orrs	r3, r1
 8000876:	6053      	str	r3, [r2, #4]
 8000878:	4aa0      	ldr	r2, [pc, #640]	; (8000afc <HAL_RCC_OscConfig+0x330>)
 800087a:	49a4      	ldr	r1, [pc, #656]	; (8000b0c <HAL_RCC_OscConfig+0x340>)
 800087c:	6813      	ldr	r3, [r2, #0]
 800087e:	400b      	ands	r3, r1
 8000880:	6921      	ldr	r1, [r4, #16]
 8000882:	430b      	orrs	r3, r1
 8000884:	6013      	str	r3, [r2, #0]
 8000886:	6813      	ldr	r3, [r2, #0]
 8000888:	4aa1      	ldr	r2, [pc, #644]	; (8000b10 <HAL_RCC_OscConfig+0x344>)
 800088a:	049b      	lsls	r3, r3, #18
 800088c:	0f5b      	lsrs	r3, r3, #29
 800088e:	40da      	lsrs	r2, r3
 8000890:	0013      	movs	r3, r2
 8000892:	4aa0      	ldr	r2, [pc, #640]	; (8000b14 <HAL_RCC_OscConfig+0x348>)
 8000894:	6013      	str	r3, [r2, #0]
 8000896:	4ba0      	ldr	r3, [pc, #640]	; (8000b18 <HAL_RCC_OscConfig+0x34c>)
 8000898:	6818      	ldr	r0, [r3, #0]
 800089a:	f7ff fde5 	bl	8000468 <HAL_InitTick>
 800089e:	2800      	cmp	r0, #0
 80008a0:	d000      	beq.n	80008a4 <HAL_RCC_OscConfig+0xd8>
 80008a2:	e096      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 80008a4:	6823      	ldr	r3, [r4, #0]
 80008a6:	071a      	lsls	r2, r3, #28
 80008a8:	d471      	bmi.n	800098e <HAL_RCC_OscConfig+0x1c2>
 80008aa:	075b      	lsls	r3, r3, #29
 80008ac:	d545      	bpl.n	800093a <HAL_RCC_OscConfig+0x16e>
 80008ae:	2338      	movs	r3, #56	; 0x38
 80008b0:	4a92      	ldr	r2, [pc, #584]	; (8000afc <HAL_RCC_OscConfig+0x330>)
 80008b2:	6891      	ldr	r1, [r2, #8]
 80008b4:	400b      	ands	r3, r1
 80008b6:	2b20      	cmp	r3, #32
 80008b8:	d100      	bne.n	80008bc <HAL_RCC_OscConfig+0xf0>
 80008ba:	e0e7      	b.n	8000a8c <HAL_RCC_OscConfig+0x2c0>
 80008bc:	2380      	movs	r3, #128	; 0x80
 80008be:	2000      	movs	r0, #0
 80008c0:	6bd1      	ldr	r1, [r2, #60]	; 0x3c
 80008c2:	055b      	lsls	r3, r3, #21
 80008c4:	4681      	mov	r9, r0
 80008c6:	4219      	tst	r1, r3
 80008c8:	d108      	bne.n	80008dc <HAL_RCC_OscConfig+0x110>
 80008ca:	6bd1      	ldr	r1, [r2, #60]	; 0x3c
 80008cc:	4319      	orrs	r1, r3
 80008ce:	63d1      	str	r1, [r2, #60]	; 0x3c
 80008d0:	6bd2      	ldr	r2, [r2, #60]	; 0x3c
 80008d2:	4013      	ands	r3, r2
 80008d4:	9301      	str	r3, [sp, #4]
 80008d6:	9b01      	ldr	r3, [sp, #4]
 80008d8:	2301      	movs	r3, #1
 80008da:	4699      	mov	r9, r3
 80008dc:	2780      	movs	r7, #128	; 0x80
 80008de:	4e8f      	ldr	r6, [pc, #572]	; (8000b1c <HAL_RCC_OscConfig+0x350>)
 80008e0:	007f      	lsls	r7, r7, #1
 80008e2:	6833      	ldr	r3, [r6, #0]
 80008e4:	423b      	tst	r3, r7
 80008e6:	d100      	bne.n	80008ea <HAL_RCC_OscConfig+0x11e>
 80008e8:	e0d9      	b.n	8000a9e <HAL_RCC_OscConfig+0x2d2>
 80008ea:	68a3      	ldr	r3, [r4, #8]
 80008ec:	2b01      	cmp	r3, #1
 80008ee:	d100      	bne.n	80008f2 <HAL_RCC_OscConfig+0x126>
 80008f0:	e128      	b.n	8000b44 <HAL_RCC_OscConfig+0x378>
 80008f2:	2b05      	cmp	r3, #5
 80008f4:	d100      	bne.n	80008f8 <HAL_RCC_OscConfig+0x12c>
 80008f6:	e1c8      	b.n	8000c8a <HAL_RCC_OscConfig+0x4be>
 80008f8:	2101      	movs	r1, #1
 80008fa:	4e80      	ldr	r6, [pc, #512]	; (8000afc <HAL_RCC_OscConfig+0x330>)
 80008fc:	6df2      	ldr	r2, [r6, #92]	; 0x5c
 80008fe:	438a      	bics	r2, r1
 8000900:	65f2      	str	r2, [r6, #92]	; 0x5c
 8000902:	6df2      	ldr	r2, [r6, #92]	; 0x5c
 8000904:	3103      	adds	r1, #3
 8000906:	438a      	bics	r2, r1
 8000908:	65f2      	str	r2, [r6, #92]	; 0x5c
 800090a:	2b00      	cmp	r3, #0
 800090c:	d000      	beq.n	8000910 <HAL_RCC_OscConfig+0x144>
 800090e:	e11d      	b.n	8000b4c <HAL_RCC_OscConfig+0x380>
 8000910:	f7ff fdf0 	bl	80004f4 <HAL_GetTick>
 8000914:	2302      	movs	r3, #2
 8000916:	0007      	movs	r7, r0
 8000918:	4698      	mov	r8, r3
 800091a:	4d81      	ldr	r5, [pc, #516]	; (8000b20 <HAL_RCC_OscConfig+0x354>)
 800091c:	e005      	b.n	800092a <HAL_RCC_OscConfig+0x15e>
 800091e:	f7ff fde9 	bl	80004f4 <HAL_GetTick>
 8000922:	1bc0      	subs	r0, r0, r7
 8000924:	42a8      	cmp	r0, r5
 8000926:	d900      	bls.n	800092a <HAL_RCC_OscConfig+0x15e>
 8000928:	e0c8      	b.n	8000abc <HAL_RCC_OscConfig+0x2f0>
 800092a:	4642      	mov	r2, r8
 800092c:	6df3      	ldr	r3, [r6, #92]	; 0x5c
 800092e:	421a      	tst	r2, r3
 8000930:	d1f5      	bne.n	800091e <HAL_RCC_OscConfig+0x152>
 8000932:	464b      	mov	r3, r9
 8000934:	2b01      	cmp	r3, #1
 8000936:	d100      	bne.n	800093a <HAL_RCC_OscConfig+0x16e>
 8000938:	e161      	b.n	8000bfe <HAL_RCC_OscConfig+0x432>
 800093a:	69e3      	ldr	r3, [r4, #28]
 800093c:	2b00      	cmp	r3, #0
 800093e:	d024      	beq.n	800098a <HAL_RCC_OscConfig+0x1be>
 8000940:	2238      	movs	r2, #56	; 0x38
 8000942:	4d6e      	ldr	r5, [pc, #440]	; (8000afc <HAL_RCC_OscConfig+0x330>)
 8000944:	68a9      	ldr	r1, [r5, #8]
 8000946:	400a      	ands	r2, r1
 8000948:	2a10      	cmp	r2, #16
 800094a:	d100      	bne.n	800094e <HAL_RCC_OscConfig+0x182>
 800094c:	e110      	b.n	8000b70 <HAL_RCC_OscConfig+0x3a4>
 800094e:	2b02      	cmp	r3, #2
 8000950:	d100      	bne.n	8000954 <HAL_RCC_OscConfig+0x188>
 8000952:	e15a      	b.n	8000c0a <HAL_RCC_OscConfig+0x43e>
 8000954:	682b      	ldr	r3, [r5, #0]
 8000956:	4a73      	ldr	r2, [pc, #460]	; (8000b24 <HAL_RCC_OscConfig+0x358>)
 8000958:	2680      	movs	r6, #128	; 0x80
 800095a:	4013      	ands	r3, r2
 800095c:	2203      	movs	r2, #3
 800095e:	602b      	str	r3, [r5, #0]
 8000960:	68eb      	ldr	r3, [r5, #12]
 8000962:	04b6      	lsls	r6, r6, #18
 8000964:	4393      	bics	r3, r2
 8000966:	60eb      	str	r3, [r5, #12]
 8000968:	68eb      	ldr	r3, [r5, #12]
 800096a:	4a6f      	ldr	r2, [pc, #444]	; (8000b28 <HAL_RCC_OscConfig+0x35c>)
 800096c:	4013      	ands	r3, r2
 800096e:	60eb      	str	r3, [r5, #12]
 8000970:	f7ff fdc0 	bl	80004f4 <HAL_GetTick>
 8000974:	0004      	movs	r4, r0
 8000976:	e005      	b.n	8000984 <HAL_RCC_OscConfig+0x1b8>
 8000978:	f7ff fdbc 	bl	80004f4 <HAL_GetTick>
 800097c:	1b00      	subs	r0, r0, r4
 800097e:	2802      	cmp	r0, #2
 8000980:	d900      	bls.n	8000984 <HAL_RCC_OscConfig+0x1b8>
 8000982:	e09b      	b.n	8000abc <HAL_RCC_OscConfig+0x2f0>
 8000984:	682b      	ldr	r3, [r5, #0]
 8000986:	4233      	tst	r3, r6
 8000988:	d1f6      	bne.n	8000978 <HAL_RCC_OscConfig+0x1ac>
 800098a:	2000      	movs	r0, #0
 800098c:	e022      	b.n	80009d4 <HAL_RCC_OscConfig+0x208>
 800098e:	2238      	movs	r2, #56	; 0x38
 8000990:	4d5a      	ldr	r5, [pc, #360]	; (8000afc <HAL_RCC_OscConfig+0x330>)
 8000992:	68a9      	ldr	r1, [r5, #8]
 8000994:	400a      	ands	r2, r1
 8000996:	2a18      	cmp	r2, #24
 8000998:	d02f      	beq.n	80009fa <HAL_RCC_OscConfig+0x22e>
 800099a:	69a3      	ldr	r3, [r4, #24]
 800099c:	2b00      	cmp	r3, #0
 800099e:	d100      	bne.n	80009a2 <HAL_RCC_OscConfig+0x1d6>
 80009a0:	e08e      	b.n	8000ac0 <HAL_RCC_OscConfig+0x2f4>
 80009a2:	2201      	movs	r2, #1
 80009a4:	6e2b      	ldr	r3, [r5, #96]	; 0x60
 80009a6:	2702      	movs	r7, #2
 80009a8:	4313      	orrs	r3, r2
 80009aa:	662b      	str	r3, [r5, #96]	; 0x60
 80009ac:	f7ff fda2 	bl	80004f4 <HAL_GetTick>
 80009b0:	0006      	movs	r6, r0
 80009b2:	e005      	b.n	80009c0 <HAL_RCC_OscConfig+0x1f4>
 80009b4:	f7ff fd9e 	bl	80004f4 <HAL_GetTick>
 80009b8:	1b80      	subs	r0, r0, r6
 80009ba:	2802      	cmp	r0, #2
 80009bc:	d900      	bls.n	80009c0 <HAL_RCC_OscConfig+0x1f4>
 80009be:	e07d      	b.n	8000abc <HAL_RCC_OscConfig+0x2f0>
 80009c0:	6e2b      	ldr	r3, [r5, #96]	; 0x60
 80009c2:	421f      	tst	r7, r3
 80009c4:	d0f6      	beq.n	80009b4 <HAL_RCC_OscConfig+0x1e8>
 80009c6:	6823      	ldr	r3, [r4, #0]
 80009c8:	e76f      	b.n	80008aa <HAL_RCC_OscConfig+0xde>
 80009ca:	68e3      	ldr	r3, [r4, #12]
 80009cc:	2b00      	cmp	r3, #0
 80009ce:	d000      	beq.n	80009d2 <HAL_RCC_OscConfig+0x206>
 80009d0:	e74b      	b.n	800086a <HAL_RCC_OscConfig+0x9e>
 80009d2:	2001      	movs	r0, #1
 80009d4:	b003      	add	sp, #12
 80009d6:	bcc0      	pop	{r6, r7}
 80009d8:	46b9      	mov	r9, r7
 80009da:	46b0      	mov	r8, r6
 80009dc:	bdf0      	pop	{r4, r5, r6, r7, pc}
 80009de:	43c9      	mvns	r1, r1
 80009e0:	078a      	lsls	r2, r1, #30
 80009e2:	d000      	beq.n	80009e6 <HAL_RCC_OscConfig+0x21a>
 80009e4:	e709      	b.n	80007fa <HAL_RCC_OscConfig+0x2e>
 80009e6:	4a45      	ldr	r2, [pc, #276]	; (8000afc <HAL_RCC_OscConfig+0x330>)
 80009e8:	6812      	ldr	r2, [r2, #0]
 80009ea:	0392      	lsls	r2, r2, #14
 80009ec:	d400      	bmi.n	80009f0 <HAL_RCC_OscConfig+0x224>
 80009ee:	e72b      	b.n	8000848 <HAL_RCC_OscConfig+0x7c>
 80009f0:	6862      	ldr	r2, [r4, #4]
 80009f2:	2a00      	cmp	r2, #0
 80009f4:	d000      	beq.n	80009f8 <HAL_RCC_OscConfig+0x22c>
 80009f6:	e727      	b.n	8000848 <HAL_RCC_OscConfig+0x7c>
 80009f8:	e7eb      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 80009fa:	6e2a      	ldr	r2, [r5, #96]	; 0x60
 80009fc:	0792      	lsls	r2, r2, #30
 80009fe:	d400      	bmi.n	8000a02 <HAL_RCC_OscConfig+0x236>
 8000a00:	e753      	b.n	80008aa <HAL_RCC_OscConfig+0xde>
 8000a02:	69a2      	ldr	r2, [r4, #24]
 8000a04:	2a00      	cmp	r2, #0
 8000a06:	d000      	beq.n	8000a0a <HAL_RCC_OscConfig+0x23e>
 8000a08:	e74f      	b.n	80008aa <HAL_RCC_OscConfig+0xde>
 8000a0a:	e7e2      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 8000a0c:	4a3b      	ldr	r2, [pc, #236]	; (8000afc <HAL_RCC_OscConfig+0x330>)
 8000a0e:	6811      	ldr	r1, [r2, #0]
 8000a10:	430b      	orrs	r3, r1
 8000a12:	6013      	str	r3, [r2, #0]
 8000a14:	f7ff fd6e 	bl	80004f4 <HAL_GetTick>
 8000a18:	2680      	movs	r6, #128	; 0x80
 8000a1a:	0005      	movs	r5, r0
 8000a1c:	4f37      	ldr	r7, [pc, #220]	; (8000afc <HAL_RCC_OscConfig+0x330>)
 8000a1e:	02b6      	lsls	r6, r6, #10
 8000a20:	e004      	b.n	8000a2c <HAL_RCC_OscConfig+0x260>
 8000a22:	f7ff fd67 	bl	80004f4 <HAL_GetTick>
 8000a26:	1b40      	subs	r0, r0, r5
 8000a28:	2864      	cmp	r0, #100	; 0x64
 8000a2a:	d847      	bhi.n	8000abc <HAL_RCC_OscConfig+0x2f0>
 8000a2c:	683b      	ldr	r3, [r7, #0]
 8000a2e:	4233      	tst	r3, r6
 8000a30:	d0f7      	beq.n	8000a22 <HAL_RCC_OscConfig+0x256>
 8000a32:	6823      	ldr	r3, [r4, #0]
 8000a34:	e708      	b.n	8000848 <HAL_RCC_OscConfig+0x7c>
 8000a36:	3b0d      	subs	r3, #13
 8000a38:	400b      	ands	r3, r1
 8000a3a:	2b02      	cmp	r3, #2
 8000a3c:	d100      	bne.n	8000a40 <HAL_RCC_OscConfig+0x274>
 8000a3e:	e0cf      	b.n	8000be0 <HAL_RCC_OscConfig+0x414>
 8000a40:	68e3      	ldr	r3, [r4, #12]
 8000a42:	4d2e      	ldr	r5, [pc, #184]	; (8000afc <HAL_RCC_OscConfig+0x330>)
 8000a44:	2b00      	cmp	r3, #0
 8000a46:	d04e      	beq.n	8000ae6 <HAL_RCC_OscConfig+0x31a>
 8000a48:	682b      	ldr	r3, [r5, #0]
 8000a4a:	4a30      	ldr	r2, [pc, #192]	; (8000b0c <HAL_RCC_OscConfig+0x340>)
 8000a4c:	2780      	movs	r7, #128	; 0x80
 8000a4e:	4013      	ands	r3, r2
 8000a50:	6922      	ldr	r2, [r4, #16]
 8000a52:	00ff      	lsls	r7, r7, #3
 8000a54:	4313      	orrs	r3, r2
 8000a56:	602b      	str	r3, [r5, #0]
 8000a58:	2380      	movs	r3, #128	; 0x80
 8000a5a:	682a      	ldr	r2, [r5, #0]
 8000a5c:	005b      	lsls	r3, r3, #1
 8000a5e:	4313      	orrs	r3, r2
 8000a60:	602b      	str	r3, [r5, #0]
 8000a62:	f7ff fd47 	bl	80004f4 <HAL_GetTick>
 8000a66:	0006      	movs	r6, r0
 8000a68:	e004      	b.n	8000a74 <HAL_RCC_OscConfig+0x2a8>
 8000a6a:	f7ff fd43 	bl	80004f4 <HAL_GetTick>
 8000a6e:	1b80      	subs	r0, r0, r6
 8000a70:	2802      	cmp	r0, #2
 8000a72:	d823      	bhi.n	8000abc <HAL_RCC_OscConfig+0x2f0>
 8000a74:	682b      	ldr	r3, [r5, #0]
 8000a76:	423b      	tst	r3, r7
 8000a78:	d0f7      	beq.n	8000a6a <HAL_RCC_OscConfig+0x29e>
 8000a7a:	686a      	ldr	r2, [r5, #4]
 8000a7c:	6963      	ldr	r3, [r4, #20]
 8000a7e:	4922      	ldr	r1, [pc, #136]	; (8000b08 <HAL_RCC_OscConfig+0x33c>)
 8000a80:	021b      	lsls	r3, r3, #8
 8000a82:	400a      	ands	r2, r1
 8000a84:	4313      	orrs	r3, r2
 8000a86:	606b      	str	r3, [r5, #4]
 8000a88:	6823      	ldr	r3, [r4, #0]
 8000a8a:	e70c      	b.n	80008a6 <HAL_RCC_OscConfig+0xda>
 8000a8c:	6dd3      	ldr	r3, [r2, #92]	; 0x5c
 8000a8e:	079b      	lsls	r3, r3, #30
 8000a90:	d400      	bmi.n	8000a94 <HAL_RCC_OscConfig+0x2c8>
 8000a92:	e752      	b.n	800093a <HAL_RCC_OscConfig+0x16e>
 8000a94:	68a3      	ldr	r3, [r4, #8]
 8000a96:	2b00      	cmp	r3, #0
 8000a98:	d000      	beq.n	8000a9c <HAL_RCC_OscConfig+0x2d0>
 8000a9a:	e74e      	b.n	800093a <HAL_RCC_OscConfig+0x16e>
 8000a9c:	e799      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 8000a9e:	6833      	ldr	r3, [r6, #0]
 8000aa0:	433b      	orrs	r3, r7
 8000aa2:	6033      	str	r3, [r6, #0]
 8000aa4:	f7ff fd26 	bl	80004f4 <HAL_GetTick>
 8000aa8:	0005      	movs	r5, r0
 8000aaa:	6833      	ldr	r3, [r6, #0]
 8000aac:	423b      	tst	r3, r7
 8000aae:	d000      	beq.n	8000ab2 <HAL_RCC_OscConfig+0x2e6>
 8000ab0:	e71b      	b.n	80008ea <HAL_RCC_OscConfig+0x11e>
 8000ab2:	f7ff fd1f 	bl	80004f4 <HAL_GetTick>
 8000ab6:	1b40      	subs	r0, r0, r5
 8000ab8:	2802      	cmp	r0, #2
 8000aba:	d9f6      	bls.n	8000aaa <HAL_RCC_OscConfig+0x2de>
 8000abc:	2003      	movs	r0, #3
 8000abe:	e789      	b.n	80009d4 <HAL_RCC_OscConfig+0x208>
 8000ac0:	2201      	movs	r2, #1
 8000ac2:	6e2b      	ldr	r3, [r5, #96]	; 0x60
 8000ac4:	2702      	movs	r7, #2
 8000ac6:	4393      	bics	r3, r2
 8000ac8:	662b      	str	r3, [r5, #96]	; 0x60
 8000aca:	f7ff fd13 	bl	80004f4 <HAL_GetTick>
 8000ace:	0006      	movs	r6, r0
 8000ad0:	e004      	b.n	8000adc <HAL_RCC_OscConfig+0x310>
 8000ad2:	f7ff fd0f 	bl	80004f4 <HAL_GetTick>
 8000ad6:	1b80      	subs	r0, r0, r6
 8000ad8:	2802      	cmp	r0, #2
 8000ada:	d8ef      	bhi.n	8000abc <HAL_RCC_OscConfig+0x2f0>
 8000adc:	6e2b      	ldr	r3, [r5, #96]	; 0x60
 8000ade:	421f      	tst	r7, r3
 8000ae0:	d1f7      	bne.n	8000ad2 <HAL_RCC_OscConfig+0x306>
 8000ae2:	6823      	ldr	r3, [r4, #0]
 8000ae4:	e6e1      	b.n	80008aa <HAL_RCC_OscConfig+0xde>
 8000ae6:	682b      	ldr	r3, [r5, #0]
 8000ae8:	4a10      	ldr	r2, [pc, #64]	; (8000b2c <HAL_RCC_OscConfig+0x360>)
 8000aea:	2780      	movs	r7, #128	; 0x80
 8000aec:	4013      	ands	r3, r2
 8000aee:	602b      	str	r3, [r5, #0]
 8000af0:	f7ff fd00 	bl	80004f4 <HAL_GetTick>
 8000af4:	00ff      	lsls	r7, r7, #3
 8000af6:	0006      	movs	r6, r0
 8000af8:	e01f      	b.n	8000b3a <HAL_RCC_OscConfig+0x36e>
 8000afa:	46c0      	nop			; (mov r8, r8)
 8000afc:	40021000 	.word	0x40021000
 8000b00:	fffeffff 	.word	0xfffeffff
 8000b04:	fffbffff 	.word	0xfffbffff
 8000b08:	ffff80ff 	.word	0xffff80ff
 8000b0c:	ffffc7ff 	.word	0xffffc7ff
 8000b10:	00f42400 	.word	0x00f42400
 8000b14:	20000000 	.word	0x20000000
 8000b18:	20000008 	.word	0x20000008
 8000b1c:	40007000 	.word	0x40007000
 8000b20:	00001388 	.word	0x00001388
 8000b24:	feffffff 	.word	0xfeffffff
 8000b28:	eefeffff 	.word	0xeefeffff
 8000b2c:	fffffeff 	.word	0xfffffeff
 8000b30:	f7ff fce0 	bl	80004f4 <HAL_GetTick>
 8000b34:	1b80      	subs	r0, r0, r6
 8000b36:	2802      	cmp	r0, #2
 8000b38:	d8c0      	bhi.n	8000abc <HAL_RCC_OscConfig+0x2f0>
 8000b3a:	682b      	ldr	r3, [r5, #0]
 8000b3c:	423b      	tst	r3, r7
 8000b3e:	d1f7      	bne.n	8000b30 <HAL_RCC_OscConfig+0x364>
 8000b40:	6823      	ldr	r3, [r4, #0]
 8000b42:	e6b0      	b.n	80008a6 <HAL_RCC_OscConfig+0xda>
 8000b44:	4956      	ldr	r1, [pc, #344]	; (8000ca0 <HAL_RCC_OscConfig+0x4d4>)
 8000b46:	6dca      	ldr	r2, [r1, #92]	; 0x5c
 8000b48:	4313      	orrs	r3, r2
 8000b4a:	65cb      	str	r3, [r1, #92]	; 0x5c
 8000b4c:	f7ff fcd2 	bl	80004f4 <HAL_GetTick>
 8000b50:	4b53      	ldr	r3, [pc, #332]	; (8000ca0 <HAL_RCC_OscConfig+0x4d4>)
 8000b52:	0006      	movs	r6, r0
 8000b54:	4698      	mov	r8, r3
 8000b56:	2702      	movs	r7, #2
 8000b58:	4d52      	ldr	r5, [pc, #328]	; (8000ca4 <HAL_RCC_OscConfig+0x4d8>)
 8000b5a:	e004      	b.n	8000b66 <HAL_RCC_OscConfig+0x39a>
 8000b5c:	f7ff fcca 	bl	80004f4 <HAL_GetTick>
 8000b60:	1b80      	subs	r0, r0, r6
 8000b62:	42a8      	cmp	r0, r5
 8000b64:	d8aa      	bhi.n	8000abc <HAL_RCC_OscConfig+0x2f0>
 8000b66:	4643      	mov	r3, r8
 8000b68:	6ddb      	ldr	r3, [r3, #92]	; 0x5c
 8000b6a:	421f      	tst	r7, r3
 8000b6c:	d0f6      	beq.n	8000b5c <HAL_RCC_OscConfig+0x390>
 8000b6e:	e6e0      	b.n	8000932 <HAL_RCC_OscConfig+0x166>
 8000b70:	2b01      	cmp	r3, #1
 8000b72:	d100      	bne.n	8000b76 <HAL_RCC_OscConfig+0x3aa>
 8000b74:	e72d      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 8000b76:	2303      	movs	r3, #3
 8000b78:	68ea      	ldr	r2, [r5, #12]
 8000b7a:	6a21      	ldr	r1, [r4, #32]
 8000b7c:	4013      	ands	r3, r2
 8000b7e:	428b      	cmp	r3, r1
 8000b80:	d000      	beq.n	8000b84 <HAL_RCC_OscConfig+0x3b8>
 8000b82:	e726      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 8000b84:	2370      	movs	r3, #112	; 0x70
 8000b86:	6a61      	ldr	r1, [r4, #36]	; 0x24
 8000b88:	4013      	ands	r3, r2
 8000b8a:	428b      	cmp	r3, r1
 8000b8c:	d000      	beq.n	8000b90 <HAL_RCC_OscConfig+0x3c4>
 8000b8e:	e720      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 8000b90:	21fe      	movs	r1, #254	; 0xfe
 8000b92:	6aa3      	ldr	r3, [r4, #40]	; 0x28
 8000b94:	01c9      	lsls	r1, r1, #7
 8000b96:	4011      	ands	r1, r2
 8000b98:	021b      	lsls	r3, r3, #8
 8000b9a:	4299      	cmp	r1, r3
 8000b9c:	d000      	beq.n	8000ba0 <HAL_RCC_OscConfig+0x3d4>
 8000b9e:	e718      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 8000ba0:	23f8      	movs	r3, #248	; 0xf8
 8000ba2:	6ae1      	ldr	r1, [r4, #44]	; 0x2c
 8000ba4:	039b      	lsls	r3, r3, #14
 8000ba6:	4013      	ands	r3, r2
 8000ba8:	428b      	cmp	r3, r1
 8000baa:	d000      	beq.n	8000bae <HAL_RCC_OscConfig+0x3e2>
 8000bac:	e711      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 8000bae:	23e0      	movs	r3, #224	; 0xe0
 8000bb0:	6b21      	ldr	r1, [r4, #48]	; 0x30
 8000bb2:	051b      	lsls	r3, r3, #20
 8000bb4:	4013      	ands	r3, r2
 8000bb6:	428b      	cmp	r3, r1
 8000bb8:	d000      	beq.n	8000bbc <HAL_RCC_OscConfig+0x3f0>
 8000bba:	e70a      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 8000bbc:	6b63      	ldr	r3, [r4, #52]	; 0x34
 8000bbe:	0f52      	lsrs	r2, r2, #29
 8000bc0:	0752      	lsls	r2, r2, #29
 8000bc2:	429a      	cmp	r2, r3
 8000bc4:	d000      	beq.n	8000bc8 <HAL_RCC_OscConfig+0x3fc>
 8000bc6:	e704      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 8000bc8:	2000      	movs	r0, #0
 8000bca:	e703      	b.n	80009d4 <HAL_RCC_OscConfig+0x208>
 8000bcc:	2180      	movs	r1, #128	; 0x80
 8000bce:	4b34      	ldr	r3, [pc, #208]	; (8000ca0 <HAL_RCC_OscConfig+0x4d4>)
 8000bd0:	02c9      	lsls	r1, r1, #11
 8000bd2:	6818      	ldr	r0, [r3, #0]
 8000bd4:	4301      	orrs	r1, r0
 8000bd6:	6019      	str	r1, [r3, #0]
 8000bd8:	6819      	ldr	r1, [r3, #0]
 8000bda:	430a      	orrs	r2, r1
 8000bdc:	601a      	str	r2, [r3, #0]
 8000bde:	e719      	b.n	8000a14 <HAL_RCC_OscConfig+0x248>
 8000be0:	6813      	ldr	r3, [r2, #0]
 8000be2:	055b      	lsls	r3, r3, #21
 8000be4:	d503      	bpl.n	8000bee <HAL_RCC_OscConfig+0x422>
 8000be6:	68e3      	ldr	r3, [r4, #12]
 8000be8:	2b00      	cmp	r3, #0
 8000bea:	d100      	bne.n	8000bee <HAL_RCC_OscConfig+0x422>
 8000bec:	e6f1      	b.n	80009d2 <HAL_RCC_OscConfig+0x206>
 8000bee:	6851      	ldr	r1, [r2, #4]
 8000bf0:	6963      	ldr	r3, [r4, #20]
 8000bf2:	482d      	ldr	r0, [pc, #180]	; (8000ca8 <HAL_RCC_OscConfig+0x4dc>)
 8000bf4:	021b      	lsls	r3, r3, #8
 8000bf6:	4001      	ands	r1, r0
 8000bf8:	430b      	orrs	r3, r1
 8000bfa:	6053      	str	r3, [r2, #4]
 8000bfc:	e64b      	b.n	8000896 <HAL_RCC_OscConfig+0xca>
 8000bfe:	4a28      	ldr	r2, [pc, #160]	; (8000ca0 <HAL_RCC_OscConfig+0x4d4>)
 8000c00:	492a      	ldr	r1, [pc, #168]	; (8000cac <HAL_RCC_OscConfig+0x4e0>)
 8000c02:	6bd3      	ldr	r3, [r2, #60]	; 0x3c
 8000c04:	400b      	ands	r3, r1
 8000c06:	63d3      	str	r3, [r2, #60]	; 0x3c
 8000c08:	e697      	b.n	800093a <HAL_RCC_OscConfig+0x16e>
 8000c0a:	682b      	ldr	r3, [r5, #0]
 8000c0c:	4a28      	ldr	r2, [pc, #160]	; (8000cb0 <HAL_RCC_OscConfig+0x4e4>)
 8000c0e:	2780      	movs	r7, #128	; 0x80
 8000c10:	4013      	ands	r3, r2
 8000c12:	602b      	str	r3, [r5, #0]
 8000c14:	f7ff fc6e 	bl	80004f4 <HAL_GetTick>
 8000c18:	04bf      	lsls	r7, r7, #18
 8000c1a:	0006      	movs	r6, r0
 8000c1c:	e005      	b.n	8000c2a <HAL_RCC_OscConfig+0x45e>
 8000c1e:	f7ff fc69 	bl	80004f4 <HAL_GetTick>
 8000c22:	1b80      	subs	r0, r0, r6
 8000c24:	2802      	cmp	r0, #2
 8000c26:	d900      	bls.n	8000c2a <HAL_RCC_OscConfig+0x45e>
 8000c28:	e748      	b.n	8000abc <HAL_RCC_OscConfig+0x2f0>
 8000c2a:	682b      	ldr	r3, [r5, #0]
 8000c2c:	423b      	tst	r3, r7
 8000c2e:	d1f6      	bne.n	8000c1e <HAL_RCC_OscConfig+0x452>
 8000c30:	6a61      	ldr	r1, [r4, #36]	; 0x24
 8000c32:	6a23      	ldr	r3, [r4, #32]
 8000c34:	68ea      	ldr	r2, [r5, #12]
 8000c36:	430b      	orrs	r3, r1
 8000c38:	491e      	ldr	r1, [pc, #120]	; (8000cb4 <HAL_RCC_OscConfig+0x4e8>)
 8000c3a:	4e19      	ldr	r6, [pc, #100]	; (8000ca0 <HAL_RCC_OscConfig+0x4d4>)
 8000c3c:	400a      	ands	r2, r1
 8000c3e:	4313      	orrs	r3, r2
 8000c40:	6ae2      	ldr	r2, [r4, #44]	; 0x2c
 8000c42:	4313      	orrs	r3, r2
 8000c44:	6b22      	ldr	r2, [r4, #48]	; 0x30
 8000c46:	4313      	orrs	r3, r2
 8000c48:	6b62      	ldr	r2, [r4, #52]	; 0x34
 8000c4a:	4313      	orrs	r3, r2
 8000c4c:	6aa2      	ldr	r2, [r4, #40]	; 0x28
 8000c4e:	0212      	lsls	r2, r2, #8
 8000c50:	4313      	orrs	r3, r2
 8000c52:	60eb      	str	r3, [r5, #12]
 8000c54:	2380      	movs	r3, #128	; 0x80
 8000c56:	682a      	ldr	r2, [r5, #0]
 8000c58:	045b      	lsls	r3, r3, #17
 8000c5a:	4313      	orrs	r3, r2
 8000c5c:	602b      	str	r3, [r5, #0]
 8000c5e:	2380      	movs	r3, #128	; 0x80
 8000c60:	68ea      	ldr	r2, [r5, #12]
 8000c62:	055b      	lsls	r3, r3, #21
 8000c64:	4313      	orrs	r3, r2
 8000c66:	60eb      	str	r3, [r5, #12]
 8000c68:	f7ff fc44 	bl	80004f4 <HAL_GetTick>
 8000c6c:	2580      	movs	r5, #128	; 0x80
 8000c6e:	0004      	movs	r4, r0
 8000c70:	04ad      	lsls	r5, r5, #18
 8000c72:	e005      	b.n	8000c80 <HAL_RCC_OscConfig+0x4b4>
 8000c74:	f7ff fc3e 	bl	80004f4 <HAL_GetTick>
 8000c78:	1b00      	subs	r0, r0, r4
 8000c7a:	2802      	cmp	r0, #2
 8000c7c:	d900      	bls.n	8000c80 <HAL_RCC_OscConfig+0x4b4>
 8000c7e:	e71d      	b.n	8000abc <HAL_RCC_OscConfig+0x2f0>
 8000c80:	6833      	ldr	r3, [r6, #0]
 8000c82:	422b      	tst	r3, r5
 8000c84:	d0f6      	beq.n	8000c74 <HAL_RCC_OscConfig+0x4a8>
 8000c86:	2000      	movs	r0, #0
 8000c88:	e6a4      	b.n	80009d4 <HAL_RCC_OscConfig+0x208>
 8000c8a:	2104      	movs	r1, #4
 8000c8c:	4b04      	ldr	r3, [pc, #16]	; (8000ca0 <HAL_RCC_OscConfig+0x4d4>)
 8000c8e:	6dda      	ldr	r2, [r3, #92]	; 0x5c
 8000c90:	430a      	orrs	r2, r1
 8000c92:	65da      	str	r2, [r3, #92]	; 0x5c
 8000c94:	6dda      	ldr	r2, [r3, #92]	; 0x5c
 8000c96:	3903      	subs	r1, #3
 8000c98:	430a      	orrs	r2, r1
 8000c9a:	65da      	str	r2, [r3, #92]	; 0x5c
 8000c9c:	e756      	b.n	8000b4c <HAL_RCC_OscConfig+0x380>
 8000c9e:	46c0      	nop			; (mov r8, r8)
 8000ca0:	40021000 	.word	0x40021000
 8000ca4:	00001388 	.word	0x00001388
 8000ca8:	ffff80ff 	.word	0xffff80ff
 8000cac:	efffffff 	.word	0xefffffff
 8000cb0:	feffffff 	.word	0xfeffffff
 8000cb4:	11c1808c 	.word	0x11c1808c

08000cb8 <HAL_RCC_GetSysClockFreq>:
 8000cb8:	2338      	movs	r3, #56	; 0x38
 8000cba:	4a20      	ldr	r2, [pc, #128]	; (8000d3c <HAL_RCC_GetSysClockFreq+0x84>)
 8000cbc:	b510      	push	{r4, lr}
 8000cbe:	6891      	ldr	r1, [r2, #8]
 8000cc0:	420b      	tst	r3, r1
 8000cc2:	d105      	bne.n	8000cd0 <HAL_RCC_GetSysClockFreq+0x18>
 8000cc4:	6813      	ldr	r3, [r2, #0]
 8000cc6:	481e      	ldr	r0, [pc, #120]	; (8000d40 <HAL_RCC_GetSysClockFreq+0x88>)
 8000cc8:	049b      	lsls	r3, r3, #18
 8000cca:	0f5b      	lsrs	r3, r3, #29
 8000ccc:	40d8      	lsrs	r0, r3
 8000cce:	bd10      	pop	{r4, pc}
 8000cd0:	6891      	ldr	r1, [r2, #8]
 8000cd2:	4019      	ands	r1, r3
 8000cd4:	2908      	cmp	r1, #8
 8000cd6:	d011      	beq.n	8000cfc <HAL_RCC_GetSysClockFreq+0x44>
 8000cd8:	6891      	ldr	r1, [r2, #8]
 8000cda:	4019      	ands	r1, r3
 8000cdc:	2910      	cmp	r1, #16
 8000cde:	d00f      	beq.n	8000d00 <HAL_RCC_GetSysClockFreq+0x48>
 8000ce0:	6891      	ldr	r1, [r2, #8]
 8000ce2:	4019      	ands	r1, r3
 8000ce4:	2920      	cmp	r1, #32
 8000ce6:	d021      	beq.n	8000d2c <HAL_RCC_GetSysClockFreq+0x74>
 8000ce8:	6890      	ldr	r0, [r2, #8]
 8000cea:	4018      	ands	r0, r3
 8000cec:	3818      	subs	r0, #24
 8000cee:	4243      	negs	r3, r0
 8000cf0:	4158      	adcs	r0, r3
 8000cf2:	23fa      	movs	r3, #250	; 0xfa
 8000cf4:	4240      	negs	r0, r0
 8000cf6:	01db      	lsls	r3, r3, #7
 8000cf8:	4018      	ands	r0, r3
 8000cfa:	e7e8      	b.n	8000cce <HAL_RCC_GetSysClockFreq+0x16>
 8000cfc:	4811      	ldr	r0, [pc, #68]	; (8000d44 <HAL_RCC_GetSysClockFreq+0x8c>)
 8000cfe:	e7e6      	b.n	8000cce <HAL_RCC_GetSysClockFreq+0x16>
 8000d00:	68d3      	ldr	r3, [r2, #12]
 8000d02:	68d1      	ldr	r1, [r2, #12]
 8000d04:	43db      	mvns	r3, r3
 8000d06:	68d0      	ldr	r0, [r2, #12]
 8000d08:	0649      	lsls	r1, r1, #25
 8000d0a:	0f49      	lsrs	r1, r1, #29
 8000d0c:	0440      	lsls	r0, r0, #17
 8000d0e:	3101      	adds	r1, #1
 8000d10:	0e44      	lsrs	r4, r0, #25
 8000d12:	079b      	lsls	r3, r3, #30
 8000d14:	d00d      	beq.n	8000d32 <HAL_RCC_GetSysClockFreq+0x7a>
 8000d16:	480a      	ldr	r0, [pc, #40]	; (8000d40 <HAL_RCC_GetSysClockFreq+0x88>)
 8000d18:	f7ff f9d0 	bl	80000bc <__udivsi3>
 8000d1c:	4360      	muls	r0, r4
 8000d1e:	4b07      	ldr	r3, [pc, #28]	; (8000d3c <HAL_RCC_GetSysClockFreq+0x84>)
 8000d20:	68d9      	ldr	r1, [r3, #12]
 8000d22:	0f49      	lsrs	r1, r1, #29
 8000d24:	3101      	adds	r1, #1
 8000d26:	f7ff f9c9 	bl	80000bc <__udivsi3>
 8000d2a:	e7d0      	b.n	8000cce <HAL_RCC_GetSysClockFreq+0x16>
 8000d2c:	2080      	movs	r0, #128	; 0x80
 8000d2e:	0200      	lsls	r0, r0, #8
 8000d30:	e7cd      	b.n	8000cce <HAL_RCC_GetSysClockFreq+0x16>
 8000d32:	4804      	ldr	r0, [pc, #16]	; (8000d44 <HAL_RCC_GetSysClockFreq+0x8c>)
 8000d34:	f7ff f9c2 	bl	80000bc <__udivsi3>
 8000d38:	4360      	muls	r0, r4
 8000d3a:	e7f0      	b.n	8000d1e <HAL_RCC_GetSysClockFreq+0x66>
 8000d3c:	40021000 	.word	0x40021000
 8000d40:	00f42400 	.word	0x00f42400
 8000d44:	007a1200 	.word	0x007a1200

08000d48 <HAL_RCC_ClockConfig>:
 8000d48:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 8000d4a:	46ce      	mov	lr, r9
 8000d4c:	4647      	mov	r7, r8
 8000d4e:	0005      	movs	r5, r0
 8000d50:	000c      	movs	r4, r1
 8000d52:	b580      	push	{r7, lr}
 8000d54:	2800      	cmp	r0, #0
 8000d56:	d026      	beq.n	8000da6 <HAL_RCC_ClockConfig+0x5e>
 8000d58:	2207      	movs	r2, #7
 8000d5a:	4e57      	ldr	r6, [pc, #348]	; (8000eb8 <HAL_RCC_ClockConfig+0x170>)
 8000d5c:	6833      	ldr	r3, [r6, #0]
 8000d5e:	4013      	ands	r3, r2
 8000d60:	428b      	cmp	r3, r1
 8000d62:	d35e      	bcc.n	8000e22 <HAL_RCC_ClockConfig+0xda>
 8000d64:	682b      	ldr	r3, [r5, #0]
 8000d66:	079a      	lsls	r2, r3, #30
 8000d68:	d50e      	bpl.n	8000d88 <HAL_RCC_ClockConfig+0x40>
 8000d6a:	075a      	lsls	r2, r3, #29
 8000d6c:	d505      	bpl.n	8000d7a <HAL_RCC_ClockConfig+0x32>
 8000d6e:	22e0      	movs	r2, #224	; 0xe0
 8000d70:	4952      	ldr	r1, [pc, #328]	; (8000ebc <HAL_RCC_ClockConfig+0x174>)
 8000d72:	01d2      	lsls	r2, r2, #7
 8000d74:	6888      	ldr	r0, [r1, #8]
 8000d76:	4302      	orrs	r2, r0
 8000d78:	608a      	str	r2, [r1, #8]
 8000d7a:	4950      	ldr	r1, [pc, #320]	; (8000ebc <HAL_RCC_ClockConfig+0x174>)
 8000d7c:	4850      	ldr	r0, [pc, #320]	; (8000ec0 <HAL_RCC_ClockConfig+0x178>)
 8000d7e:	688a      	ldr	r2, [r1, #8]
 8000d80:	4002      	ands	r2, r0
 8000d82:	68a8      	ldr	r0, [r5, #8]
 8000d84:	4302      	orrs	r2, r0
 8000d86:	608a      	str	r2, [r1, #8]
 8000d88:	07db      	lsls	r3, r3, #31
 8000d8a:	d52b      	bpl.n	8000de4 <HAL_RCC_ClockConfig+0x9c>
 8000d8c:	686b      	ldr	r3, [r5, #4]
 8000d8e:	4a4b      	ldr	r2, [pc, #300]	; (8000ebc <HAL_RCC_ClockConfig+0x174>)
 8000d90:	2b01      	cmp	r3, #1
 8000d92:	d100      	bne.n	8000d96 <HAL_RCC_ClockConfig+0x4e>
 8000d94:	e07c      	b.n	8000e90 <HAL_RCC_ClockConfig+0x148>
 8000d96:	2b02      	cmp	r3, #2
 8000d98:	d007      	beq.n	8000daa <HAL_RCC_ClockConfig+0x62>
 8000d9a:	2b00      	cmp	r3, #0
 8000d9c:	d000      	beq.n	8000da0 <HAL_RCC_ClockConfig+0x58>
 8000d9e:	e07d      	b.n	8000e9c <HAL_RCC_ClockConfig+0x154>
 8000da0:	6812      	ldr	r2, [r2, #0]
 8000da2:	0552      	lsls	r2, r2, #21
 8000da4:	d404      	bmi.n	8000db0 <HAL_RCC_ClockConfig+0x68>
 8000da6:	2001      	movs	r0, #1
 8000da8:	e037      	b.n	8000e1a <HAL_RCC_ClockConfig+0xd2>
 8000daa:	6812      	ldr	r2, [r2, #0]
 8000dac:	0192      	lsls	r2, r2, #6
 8000dae:	d5fa      	bpl.n	8000da6 <HAL_RCC_ClockConfig+0x5e>
 8000db0:	2107      	movs	r1, #7
 8000db2:	4e42      	ldr	r6, [pc, #264]	; (8000ebc <HAL_RCC_ClockConfig+0x174>)
 8000db4:	68b2      	ldr	r2, [r6, #8]
 8000db6:	438a      	bics	r2, r1
 8000db8:	4313      	orrs	r3, r2
 8000dba:	60b3      	str	r3, [r6, #8]
 8000dbc:	f7ff fb9a 	bl	80004f4 <HAL_GetTick>
 8000dc0:	2338      	movs	r3, #56	; 0x38
 8000dc2:	4698      	mov	r8, r3
 8000dc4:	4b3f      	ldr	r3, [pc, #252]	; (8000ec4 <HAL_RCC_ClockConfig+0x17c>)
 8000dc6:	0007      	movs	r7, r0
 8000dc8:	4699      	mov	r9, r3
 8000dca:	e004      	b.n	8000dd6 <HAL_RCC_ClockConfig+0x8e>
 8000dcc:	f7ff fb92 	bl	80004f4 <HAL_GetTick>
 8000dd0:	1bc0      	subs	r0, r0, r7
 8000dd2:	4548      	cmp	r0, r9
 8000dd4:	d83b      	bhi.n	8000e4e <HAL_RCC_ClockConfig+0x106>
 8000dd6:	4643      	mov	r3, r8
 8000dd8:	68b2      	ldr	r2, [r6, #8]
 8000dda:	401a      	ands	r2, r3
 8000ddc:	686b      	ldr	r3, [r5, #4]
 8000dde:	00db      	lsls	r3, r3, #3
 8000de0:	429a      	cmp	r2, r3
 8000de2:	d1f3      	bne.n	8000dcc <HAL_RCC_ClockConfig+0x84>
 8000de4:	2207      	movs	r2, #7
 8000de6:	4e34      	ldr	r6, [pc, #208]	; (8000eb8 <HAL_RCC_ClockConfig+0x170>)
 8000de8:	6833      	ldr	r3, [r6, #0]
 8000dea:	4013      	ands	r3, r2
 8000dec:	42a3      	cmp	r3, r4
 8000dee:	d838      	bhi.n	8000e62 <HAL_RCC_ClockConfig+0x11a>
 8000df0:	682b      	ldr	r3, [r5, #0]
 8000df2:	075b      	lsls	r3, r3, #29
 8000df4:	d42d      	bmi.n	8000e52 <HAL_RCC_ClockConfig+0x10a>
 8000df6:	f7ff ff5f 	bl	8000cb8 <HAL_RCC_GetSysClockFreq>
 8000dfa:	4b30      	ldr	r3, [pc, #192]	; (8000ebc <HAL_RCC_ClockConfig+0x174>)
 8000dfc:	4a32      	ldr	r2, [pc, #200]	; (8000ec8 <HAL_RCC_ClockConfig+0x180>)
 8000dfe:	689b      	ldr	r3, [r3, #8]
 8000e00:	051b      	lsls	r3, r3, #20
 8000e02:	0f1b      	lsrs	r3, r3, #28
 8000e04:	009b      	lsls	r3, r3, #2
 8000e06:	589b      	ldr	r3, [r3, r2]
 8000e08:	221f      	movs	r2, #31
 8000e0a:	4013      	ands	r3, r2
 8000e0c:	40d8      	lsrs	r0, r3
 8000e0e:	4b2f      	ldr	r3, [pc, #188]	; (8000ecc <HAL_RCC_ClockConfig+0x184>)
 8000e10:	6018      	str	r0, [r3, #0]
 8000e12:	4b2f      	ldr	r3, [pc, #188]	; (8000ed0 <HAL_RCC_ClockConfig+0x188>)
 8000e14:	6818      	ldr	r0, [r3, #0]
 8000e16:	f7ff fb27 	bl	8000468 <HAL_InitTick>
 8000e1a:	bcc0      	pop	{r6, r7}
 8000e1c:	46b9      	mov	r9, r7
 8000e1e:	46b0      	mov	r8, r6
 8000e20:	bdf8      	pop	{r3, r4, r5, r6, r7, pc}
 8000e22:	6833      	ldr	r3, [r6, #0]
 8000e24:	4393      	bics	r3, r2
 8000e26:	430b      	orrs	r3, r1
 8000e28:	6033      	str	r3, [r6, #0]
 8000e2a:	f7ff fb63 	bl	80004f4 <HAL_GetTick>
 8000e2e:	2307      	movs	r3, #7
 8000e30:	4698      	mov	r8, r3
 8000e32:	4b24      	ldr	r3, [pc, #144]	; (8000ec4 <HAL_RCC_ClockConfig+0x17c>)
 8000e34:	0007      	movs	r7, r0
 8000e36:	4699      	mov	r9, r3
 8000e38:	4642      	mov	r2, r8
 8000e3a:	6833      	ldr	r3, [r6, #0]
 8000e3c:	4013      	ands	r3, r2
 8000e3e:	42a3      	cmp	r3, r4
 8000e40:	d100      	bne.n	8000e44 <HAL_RCC_ClockConfig+0xfc>
 8000e42:	e78f      	b.n	8000d64 <HAL_RCC_ClockConfig+0x1c>
 8000e44:	f7ff fb56 	bl	80004f4 <HAL_GetTick>
 8000e48:	1bc0      	subs	r0, r0, r7
 8000e4a:	4548      	cmp	r0, r9
 8000e4c:	d9f4      	bls.n	8000e38 <HAL_RCC_ClockConfig+0xf0>
 8000e4e:	2003      	movs	r0, #3
 8000e50:	e7e3      	b.n	8000e1a <HAL_RCC_ClockConfig+0xd2>
 8000e52:	4a1a      	ldr	r2, [pc, #104]	; (8000ebc <HAL_RCC_ClockConfig+0x174>)
 8000e54:	491f      	ldr	r1, [pc, #124]	; (8000ed4 <HAL_RCC_ClockConfig+0x18c>)
 8000e56:	6893      	ldr	r3, [r2, #8]
 8000e58:	400b      	ands	r3, r1
 8000e5a:	68e9      	ldr	r1, [r5, #12]
 8000e5c:	430b      	orrs	r3, r1
 8000e5e:	6093      	str	r3, [r2, #8]
 8000e60:	e7c9      	b.n	8000df6 <HAL_RCC_ClockConfig+0xae>
 8000e62:	6833      	ldr	r3, [r6, #0]
 8000e64:	4393      	bics	r3, r2
 8000e66:	4323      	orrs	r3, r4
 8000e68:	6033      	str	r3, [r6, #0]
 8000e6a:	f7ff fb43 	bl	80004f4 <HAL_GetTick>
 8000e6e:	2307      	movs	r3, #7
 8000e70:	4698      	mov	r8, r3
 8000e72:	4b14      	ldr	r3, [pc, #80]	; (8000ec4 <HAL_RCC_ClockConfig+0x17c>)
 8000e74:	0007      	movs	r7, r0
 8000e76:	4699      	mov	r9, r3
 8000e78:	4642      	mov	r2, r8
 8000e7a:	6833      	ldr	r3, [r6, #0]
 8000e7c:	4013      	ands	r3, r2
 8000e7e:	42a3      	cmp	r3, r4
 8000e80:	d0b6      	beq.n	8000df0 <HAL_RCC_ClockConfig+0xa8>
 8000e82:	f7ff fb37 	bl	80004f4 <HAL_GetTick>
 8000e86:	1bc0      	subs	r0, r0, r7
 8000e88:	4548      	cmp	r0, r9
 8000e8a:	d9f5      	bls.n	8000e78 <HAL_RCC_ClockConfig+0x130>
 8000e8c:	2003      	movs	r0, #3
 8000e8e:	e7c4      	b.n	8000e1a <HAL_RCC_ClockConfig+0xd2>
 8000e90:	6812      	ldr	r2, [r2, #0]
 8000e92:	0392      	lsls	r2, r2, #14
 8000e94:	d500      	bpl.n	8000e98 <HAL_RCC_ClockConfig+0x150>
 8000e96:	e78b      	b.n	8000db0 <HAL_RCC_ClockConfig+0x68>
 8000e98:	2001      	movs	r0, #1
 8000e9a:	e7be      	b.n	8000e1a <HAL_RCC_ClockConfig+0xd2>
 8000e9c:	2b03      	cmp	r3, #3
 8000e9e:	d005      	beq.n	8000eac <HAL_RCC_ClockConfig+0x164>
 8000ea0:	6dd2      	ldr	r2, [r2, #92]	; 0x5c
 8000ea2:	0792      	lsls	r2, r2, #30
 8000ea4:	d500      	bpl.n	8000ea8 <HAL_RCC_ClockConfig+0x160>
 8000ea6:	e783      	b.n	8000db0 <HAL_RCC_ClockConfig+0x68>
 8000ea8:	2001      	movs	r0, #1
 8000eaa:	e7b6      	b.n	8000e1a <HAL_RCC_ClockConfig+0xd2>
 8000eac:	6e12      	ldr	r2, [r2, #96]	; 0x60
 8000eae:	0792      	lsls	r2, r2, #30
 8000eb0:	d500      	bpl.n	8000eb4 <HAL_RCC_ClockConfig+0x16c>
 8000eb2:	e77d      	b.n	8000db0 <HAL_RCC_ClockConfig+0x68>
 8000eb4:	2001      	movs	r0, #1
 8000eb6:	e7b0      	b.n	8000e1a <HAL_RCC_ClockConfig+0xd2>
 8000eb8:	40022000 	.word	0x40022000
 8000ebc:	40021000 	.word	0x40021000
 8000ec0:	fffff0ff 	.word	0xfffff0ff
 8000ec4:	00001388 	.word	0x00001388
 8000ec8:	08001224 	.word	0x08001224
 8000ecc:	20000000 	.word	0x20000000
 8000ed0:	20000008 	.word	0x20000008
 8000ed4:	ffff8fff 	.word	0xffff8fff

08000ed8 <HAL_SPI_Init>:
 8000ed8:	b5f0      	push	{r4, r5, r6, r7, lr}
 8000eda:	46d6      	mov	lr, sl
 8000edc:	464f      	mov	r7, r9
 8000ede:	4646      	mov	r6, r8
 8000ee0:	0004      	movs	r4, r0
 8000ee2:	b5c0      	push	{r6, r7, lr}
 8000ee4:	2800      	cmp	r0, #0
 8000ee6:	d100      	bne.n	8000eea <HAL_SPI_Init+0x12>
 8000ee8:	e08e      	b.n	8001008 <HAL_SPI_Init+0x130>
 8000eea:	6a45      	ldr	r5, [r0, #36]	; 0x24
 8000eec:	2d00      	cmp	r5, #0
 8000eee:	d065      	beq.n	8000fbc <HAL_SPI_Init+0xe4>
 8000ef0:	2300      	movs	r3, #0
 8000ef2:	6103      	str	r3, [r0, #16]
 8000ef4:	6143      	str	r3, [r0, #20]
 8000ef6:	2300      	movs	r3, #0
 8000ef8:	62a3      	str	r3, [r4, #40]	; 0x28
 8000efa:	335d      	adds	r3, #93	; 0x5d
 8000efc:	5ce3      	ldrb	r3, [r4, r3]
 8000efe:	b2da      	uxtb	r2, r3
 8000f00:	2b00      	cmp	r3, #0
 8000f02:	d068      	beq.n	8000fd6 <HAL_SPI_Init+0xfe>
 8000f04:	235d      	movs	r3, #93	; 0x5d
 8000f06:	2202      	movs	r2, #2
 8000f08:	2140      	movs	r1, #64	; 0x40
 8000f0a:	54e2      	strb	r2, [r4, r3]
 8000f0c:	6822      	ldr	r2, [r4, #0]
 8000f0e:	68e0      	ldr	r0, [r4, #12]
 8000f10:	6813      	ldr	r3, [r2, #0]
 8000f12:	438b      	bics	r3, r1
 8000f14:	6013      	str	r3, [r2, #0]
 8000f16:	23e0      	movs	r3, #224	; 0xe0
 8000f18:	00db      	lsls	r3, r3, #3
 8000f1a:	4298      	cmp	r0, r3
 8000f1c:	d962      	bls.n	8000fe4 <HAL_SPI_Init+0x10c>
 8000f1e:	23f0      	movs	r3, #240	; 0xf0
 8000f20:	011b      	lsls	r3, r3, #4
 8000f22:	4298      	cmp	r0, r3
 8000f24:	d000      	beq.n	8000f28 <HAL_SPI_Init+0x50>
 8000f26:	e071      	b.n	800100c <HAL_SPI_Init+0x134>
 8000f28:	2380      	movs	r3, #128	; 0x80
 8000f2a:	6aa1      	ldr	r1, [r4, #40]	; 0x28
 8000f2c:	019b      	lsls	r3, r3, #6
 8000f2e:	4019      	ands	r1, r3
 8000f30:	2300      	movs	r3, #0
 8000f32:	468c      	mov	ip, r1
 8000f34:	469a      	mov	sl, r3
 8000f36:	2382      	movs	r3, #130	; 0x82
 8000f38:	6866      	ldr	r6, [r4, #4]
 8000f3a:	005b      	lsls	r3, r3, #1
 8000f3c:	4033      	ands	r3, r6
 8000f3e:	2684      	movs	r6, #132	; 0x84
 8000f40:	68a7      	ldr	r7, [r4, #8]
 8000f42:	0236      	lsls	r6, r6, #8
 8000f44:	403e      	ands	r6, r7
 8000f46:	4333      	orrs	r3, r6
 8000f48:	2602      	movs	r6, #2
 8000f4a:	6927      	ldr	r7, [r4, #16]
 8000f4c:	69a1      	ldr	r1, [r4, #24]
 8000f4e:	403e      	ands	r6, r7
 8000f50:	4333      	orrs	r3, r6
 8000f52:	2601      	movs	r6, #1
 8000f54:	6967      	ldr	r7, [r4, #20]
 8000f56:	46b1      	mov	r9, r6
 8000f58:	4037      	ands	r7, r6
 8000f5a:	433b      	orrs	r3, r7
 8000f5c:	2780      	movs	r7, #128	; 0x80
 8000f5e:	00bf      	lsls	r7, r7, #2
 8000f60:	400f      	ands	r7, r1
 8000f62:	433b      	orrs	r3, r7
 8000f64:	2738      	movs	r7, #56	; 0x38
 8000f66:	69e6      	ldr	r6, [r4, #28]
 8000f68:	0c09      	lsrs	r1, r1, #16
 8000f6a:	4037      	ands	r7, r6
 8000f6c:	2680      	movs	r6, #128	; 0x80
 8000f6e:	433b      	orrs	r3, r7
 8000f70:	0037      	movs	r7, r6
 8000f72:	6a26      	ldr	r6, [r4, #32]
 8000f74:	4037      	ands	r7, r6
 8000f76:	4666      	mov	r6, ip
 8000f78:	433b      	orrs	r3, r7
 8000f7a:	4333      	orrs	r3, r6
 8000f7c:	6013      	str	r3, [r2, #0]
 8000f7e:	2308      	movs	r3, #8
 8000f80:	6b66      	ldr	r6, [r4, #52]	; 0x34
 8000f82:	4657      	mov	r7, sl
 8000f84:	4033      	ands	r3, r6
 8000f86:	26f0      	movs	r6, #240	; 0xf0
 8000f88:	0136      	lsls	r6, r6, #4
 8000f8a:	4030      	ands	r0, r6
 8000f8c:	4318      	orrs	r0, r3
 8000f8e:	2304      	movs	r3, #4
 8000f90:	4019      	ands	r1, r3
 8000f92:	4308      	orrs	r0, r1
 8000f94:	2110      	movs	r1, #16
 8000f96:	400d      	ands	r5, r1
 8000f98:	4305      	orrs	r5, r0
 8000f9a:	432f      	orrs	r7, r5
 8000f9c:	6057      	str	r7, [r2, #4]
 8000f9e:	69d3      	ldr	r3, [r2, #28]
 8000fa0:	491c      	ldr	r1, [pc, #112]	; (8001014 <HAL_SPI_Init+0x13c>)
 8000fa2:	2000      	movs	r0, #0
 8000fa4:	400b      	ands	r3, r1
 8000fa6:	61d3      	str	r3, [r2, #28]
 8000fa8:	2300      	movs	r3, #0
 8000faa:	464a      	mov	r2, r9
 8000fac:	6623      	str	r3, [r4, #96]	; 0x60
 8000fae:	335d      	adds	r3, #93	; 0x5d
 8000fb0:	54e2      	strb	r2, [r4, r3]
 8000fb2:	bce0      	pop	{r5, r6, r7}
 8000fb4:	46ba      	mov	sl, r7
 8000fb6:	46b1      	mov	r9, r6
 8000fb8:	46a8      	mov	r8, r5
 8000fba:	bdf0      	pop	{r4, r5, r6, r7, pc}
 8000fbc:	2382      	movs	r3, #130	; 0x82
 8000fbe:	6842      	ldr	r2, [r0, #4]
 8000fc0:	005b      	lsls	r3, r3, #1
 8000fc2:	429a      	cmp	r2, r3
 8000fc4:	d097      	beq.n	8000ef6 <HAL_SPI_Init+0x1e>
 8000fc6:	2300      	movs	r3, #0
 8000fc8:	61c5      	str	r5, [r0, #28]
 8000fca:	62a3      	str	r3, [r4, #40]	; 0x28
 8000fcc:	335d      	adds	r3, #93	; 0x5d
 8000fce:	5ce3      	ldrb	r3, [r4, r3]
 8000fd0:	b2da      	uxtb	r2, r3
 8000fd2:	2b00      	cmp	r3, #0
 8000fd4:	d196      	bne.n	8000f04 <HAL_SPI_Init+0x2c>
 8000fd6:	335c      	adds	r3, #92	; 0x5c
 8000fd8:	0020      	movs	r0, r4
 8000fda:	54e2      	strb	r2, [r4, r3]
 8000fdc:	f7ff f9d8 	bl	8000390 <HAL_SPI_MspInit>
 8000fe0:	6a65      	ldr	r5, [r4, #36]	; 0x24
 8000fe2:	e78f      	b.n	8000f04 <HAL_SPI_Init+0x2c>
 8000fe4:	4298      	cmp	r0, r3
 8000fe6:	d006      	beq.n	8000ff6 <HAL_SPI_Init+0x11e>
 8000fe8:	2380      	movs	r3, #128	; 0x80
 8000fea:	015b      	lsls	r3, r3, #5
 8000fec:	469a      	mov	sl, r3
 8000fee:	2300      	movs	r3, #0
 8000ff0:	469c      	mov	ip, r3
 8000ff2:	62a3      	str	r3, [r4, #40]	; 0x28
 8000ff4:	e79f      	b.n	8000f36 <HAL_SPI_Init+0x5e>
 8000ff6:	2380      	movs	r3, #128	; 0x80
 8000ff8:	6aa1      	ldr	r1, [r4, #40]	; 0x28
 8000ffa:	019b      	lsls	r3, r3, #6
 8000ffc:	4019      	ands	r1, r3
 8000ffe:	2380      	movs	r3, #128	; 0x80
 8001000:	015b      	lsls	r3, r3, #5
 8001002:	468c      	mov	ip, r1
 8001004:	469a      	mov	sl, r3
 8001006:	e796      	b.n	8000f36 <HAL_SPI_Init+0x5e>
 8001008:	2001      	movs	r0, #1
 800100a:	e7d2      	b.n	8000fb2 <HAL_SPI_Init+0xda>
 800100c:	2300      	movs	r3, #0
 800100e:	469a      	mov	sl, r3
 8001010:	e7ed      	b.n	8000fee <HAL_SPI_Init+0x116>
 8001012:	46c0      	nop			; (mov r8, r8)
 8001014:	fffff7ff 	.word	0xfffff7ff

08001018 <atexit>:
 8001018:	b510      	push	{r4, lr}
 800101a:	0001      	movs	r1, r0
 800101c:	2300      	movs	r3, #0
 800101e:	2200      	movs	r2, #0
 8001020:	2000      	movs	r0, #0
 8001022:	f000 f88b 	bl	800113c <__register_exitproc>
 8001026:	bd10      	pop	{r4, pc}

08001028 <__libc_fini_array>:
 8001028:	b570      	push	{r4, r5, r6, lr}
 800102a:	4d07      	ldr	r5, [pc, #28]	; (8001048 <__libc_fini_array+0x20>)
 800102c:	4c07      	ldr	r4, [pc, #28]	; (800104c <__libc_fini_array+0x24>)
 800102e:	1b64      	subs	r4, r4, r5
 8001030:	10a4      	asrs	r4, r4, #2
 8001032:	d005      	beq.n	8001040 <__libc_fini_array+0x18>
 8001034:	3c01      	subs	r4, #1
 8001036:	00a3      	lsls	r3, r4, #2
 8001038:	58eb      	ldr	r3, [r5, r3]
 800103a:	4798      	blx	r3
 800103c:	2c00      	cmp	r4, #0
 800103e:	d1f9      	bne.n	8001034 <__libc_fini_array+0xc>
 8001040:	f000 f8ea 	bl	8001218 <_fini>
 8001044:	bd70      	pop	{r4, r5, r6, pc}
 8001046:	46c0      	nop			; (mov r8, r8)
 8001048:	08001270 	.word	0x08001270
 800104c:	08001274 	.word	0x08001274

08001050 <__libc_init_array>:
 8001050:	b570      	push	{r4, r5, r6, lr}
 8001052:	4d0c      	ldr	r5, [pc, #48]	; (8001084 <__libc_init_array+0x34>)
 8001054:	4e0c      	ldr	r6, [pc, #48]	; (8001088 <__libc_init_array+0x38>)
 8001056:	1b76      	subs	r6, r6, r5
 8001058:	10b6      	asrs	r6, r6, #2
 800105a:	d005      	beq.n	8001068 <__libc_init_array+0x18>
 800105c:	2400      	movs	r4, #0
 800105e:	cd08      	ldmia	r5!, {r3}
 8001060:	3401      	adds	r4, #1
 8001062:	4798      	blx	r3
 8001064:	42a6      	cmp	r6, r4
 8001066:	d1fa      	bne.n	800105e <__libc_init_array+0xe>
 8001068:	f000 f8d0 	bl	800120c <_init>
 800106c:	4d07      	ldr	r5, [pc, #28]	; (800108c <__libc_init_array+0x3c>)
 800106e:	4e08      	ldr	r6, [pc, #32]	; (8001090 <__libc_init_array+0x40>)
 8001070:	1b76      	subs	r6, r6, r5
 8001072:	10b6      	asrs	r6, r6, #2
 8001074:	d005      	beq.n	8001082 <__libc_init_array+0x32>
 8001076:	2400      	movs	r4, #0
 8001078:	cd08      	ldmia	r5!, {r3}
 800107a:	3401      	adds	r4, #1
 800107c:	4798      	blx	r3
 800107e:	42a6      	cmp	r6, r4
 8001080:	d1fa      	bne.n	8001078 <__libc_init_array+0x28>
 8001082:	bd70      	pop	{r4, r5, r6, pc}
 8001084:	08001268 	.word	0x08001268
 8001088:	08001268 	.word	0x08001268
 800108c:	08001268 	.word	0x08001268
 8001090:	08001270 	.word	0x08001270

08001094 <memset>:
 8001094:	b5f0      	push	{r4, r5, r6, r7, lr}
 8001096:	0005      	movs	r5, r0
 8001098:	0783      	lsls	r3, r0, #30
 800109a:	d049      	beq.n	8001130 <memset+0x9c>
 800109c:	1e54      	subs	r4, r2, #1
 800109e:	2a00      	cmp	r2, #0
 80010a0:	d045      	beq.n	800112e <memset+0x9a>
 80010a2:	0003      	movs	r3, r0
 80010a4:	2603      	movs	r6, #3
 80010a6:	b2ca      	uxtb	r2, r1
 80010a8:	e002      	b.n	80010b0 <memset+0x1c>
 80010aa:	3501      	adds	r5, #1
 80010ac:	3c01      	subs	r4, #1
 80010ae:	d33e      	bcc.n	800112e <memset+0x9a>
 80010b0:	3301      	adds	r3, #1
 80010b2:	702a      	strb	r2, [r5, #0]
 80010b4:	4233      	tst	r3, r6
 80010b6:	d1f8      	bne.n	80010aa <memset+0x16>
 80010b8:	2c03      	cmp	r4, #3
 80010ba:	d930      	bls.n	800111e <memset+0x8a>
 80010bc:	22ff      	movs	r2, #255	; 0xff
 80010be:	400a      	ands	r2, r1
 80010c0:	0215      	lsls	r5, r2, #8
 80010c2:	4315      	orrs	r5, r2
 80010c4:	042a      	lsls	r2, r5, #16
 80010c6:	4315      	orrs	r5, r2
 80010c8:	2c0f      	cmp	r4, #15
 80010ca:	d934      	bls.n	8001136 <memset+0xa2>
 80010cc:	0027      	movs	r7, r4
 80010ce:	3f10      	subs	r7, #16
 80010d0:	093f      	lsrs	r7, r7, #4
 80010d2:	013e      	lsls	r6, r7, #4
 80010d4:	46b4      	mov	ip, r6
 80010d6:	001e      	movs	r6, r3
 80010d8:	001a      	movs	r2, r3
 80010da:	3610      	adds	r6, #16
 80010dc:	4466      	add	r6, ip
 80010de:	6015      	str	r5, [r2, #0]
 80010e0:	6055      	str	r5, [r2, #4]
 80010e2:	6095      	str	r5, [r2, #8]
 80010e4:	60d5      	str	r5, [r2, #12]
 80010e6:	3210      	adds	r2, #16
 80010e8:	42b2      	cmp	r2, r6
 80010ea:	d1f8      	bne.n	80010de <memset+0x4a>
 80010ec:	3701      	adds	r7, #1
 80010ee:	013f      	lsls	r7, r7, #4
 80010f0:	19db      	adds	r3, r3, r7
 80010f2:	270f      	movs	r7, #15
 80010f4:	220c      	movs	r2, #12
 80010f6:	4027      	ands	r7, r4
 80010f8:	4022      	ands	r2, r4
 80010fa:	003c      	movs	r4, r7
 80010fc:	2a00      	cmp	r2, #0
 80010fe:	d00e      	beq.n	800111e <memset+0x8a>
 8001100:	1f3e      	subs	r6, r7, #4
 8001102:	08b6      	lsrs	r6, r6, #2
 8001104:	00b4      	lsls	r4, r6, #2
 8001106:	46a4      	mov	ip, r4
 8001108:	001a      	movs	r2, r3
 800110a:	1d1c      	adds	r4, r3, #4
 800110c:	4464      	add	r4, ip
 800110e:	c220      	stmia	r2!, {r5}
 8001110:	42a2      	cmp	r2, r4
 8001112:	d1fc      	bne.n	800110e <memset+0x7a>
 8001114:	2403      	movs	r4, #3
 8001116:	3601      	adds	r6, #1
 8001118:	00b6      	lsls	r6, r6, #2
 800111a:	199b      	adds	r3, r3, r6
 800111c:	403c      	ands	r4, r7
 800111e:	2c00      	cmp	r4, #0
 8001120:	d005      	beq.n	800112e <memset+0x9a>
 8001122:	b2c9      	uxtb	r1, r1
 8001124:	191c      	adds	r4, r3, r4
 8001126:	7019      	strb	r1, [r3, #0]
 8001128:	3301      	adds	r3, #1
 800112a:	429c      	cmp	r4, r3
 800112c:	d1fb      	bne.n	8001126 <memset+0x92>
 800112e:	bdf0      	pop	{r4, r5, r6, r7, pc}
 8001130:	0003      	movs	r3, r0
 8001132:	0014      	movs	r4, r2
 8001134:	e7c0      	b.n	80010b8 <memset+0x24>
 8001136:	0027      	movs	r7, r4
 8001138:	e7e2      	b.n	8001100 <memset+0x6c>
 800113a:	46c0      	nop			; (mov r8, r8)

0800113c <__register_exitproc>:
 800113c:	b5f0      	push	{r4, r5, r6, r7, lr}
 800113e:	46d6      	mov	lr, sl
 8001140:	464f      	mov	r7, r9
 8001142:	4646      	mov	r6, r8
 8001144:	b5c0      	push	{r6, r7, lr}
 8001146:	4f27      	ldr	r7, [pc, #156]	; (80011e4 <__register_exitproc+0xa8>)
 8001148:	b082      	sub	sp, #8
 800114a:	0006      	movs	r6, r0
 800114c:	6838      	ldr	r0, [r7, #0]
 800114e:	4691      	mov	r9, r2
 8001150:	4698      	mov	r8, r3
 8001152:	468a      	mov	sl, r1
 8001154:	f000 f856 	bl	8001204 <__retarget_lock_acquire_recursive>
 8001158:	4b23      	ldr	r3, [pc, #140]	; (80011e8 <__register_exitproc+0xac>)
 800115a:	681b      	ldr	r3, [r3, #0]
 800115c:	9301      	str	r3, [sp, #4]
 800115e:	23a4      	movs	r3, #164	; 0xa4
 8001160:	9a01      	ldr	r2, [sp, #4]
 8001162:	005b      	lsls	r3, r3, #1
 8001164:	58d5      	ldr	r5, [r2, r3]
 8001166:	2d00      	cmp	r5, #0
 8001168:	d031      	beq.n	80011ce <__register_exitproc+0x92>
 800116a:	686c      	ldr	r4, [r5, #4]
 800116c:	6838      	ldr	r0, [r7, #0]
 800116e:	2c1f      	cmp	r4, #31
 8001170:	dc32      	bgt.n	80011d8 <__register_exitproc+0x9c>
 8001172:	2e00      	cmp	r6, #0
 8001174:	d10e      	bne.n	8001194 <__register_exitproc+0x58>
 8001176:	1c63      	adds	r3, r4, #1
 8001178:	606b      	str	r3, [r5, #4]
 800117a:	4653      	mov	r3, sl
 800117c:	3402      	adds	r4, #2
 800117e:	00a4      	lsls	r4, r4, #2
 8001180:	5163      	str	r3, [r4, r5]
 8001182:	f000 f841 	bl	8001208 <__retarget_lock_release_recursive>
 8001186:	2000      	movs	r0, #0
 8001188:	b002      	add	sp, #8
 800118a:	bce0      	pop	{r5, r6, r7}
 800118c:	46ba      	mov	sl, r7
 800118e:	46b1      	mov	r9, r6
 8001190:	46a8      	mov	r8, r5
 8001192:	bdf0      	pop	{r4, r5, r6, r7, pc}
 8001194:	2288      	movs	r2, #136	; 0x88
 8001196:	4649      	mov	r1, r9
 8001198:	00a3      	lsls	r3, r4, #2
 800119a:	18eb      	adds	r3, r5, r3
 800119c:	5099      	str	r1, [r3, r2]
 800119e:	21c4      	movs	r1, #196	; 0xc4
 80011a0:	0049      	lsls	r1, r1, #1
 80011a2:	468c      	mov	ip, r1
 80011a4:	44ac      	add	ip, r5
 80011a6:	4661      	mov	r1, ip
 80011a8:	3a87      	subs	r2, #135	; 0x87
 80011aa:	40a2      	lsls	r2, r4
 80011ac:	4667      	mov	r7, ip
 80011ae:	6809      	ldr	r1, [r1, #0]
 80011b0:	4311      	orrs	r1, r2
 80011b2:	6039      	str	r1, [r7, #0]
 80011b4:	2184      	movs	r1, #132	; 0x84
 80011b6:	4647      	mov	r7, r8
 80011b8:	0049      	lsls	r1, r1, #1
 80011ba:	505f      	str	r7, [r3, r1]
 80011bc:	2e02      	cmp	r6, #2
 80011be:	d1da      	bne.n	8001176 <__register_exitproc+0x3a>
 80011c0:	002b      	movs	r3, r5
 80011c2:	338d      	adds	r3, #141	; 0x8d
 80011c4:	33ff      	adds	r3, #255	; 0xff
 80011c6:	6819      	ldr	r1, [r3, #0]
 80011c8:	430a      	orrs	r2, r1
 80011ca:	601a      	str	r2, [r3, #0]
 80011cc:	e7d3      	b.n	8001176 <__register_exitproc+0x3a>
 80011ce:	0015      	movs	r5, r2
 80011d0:	354d      	adds	r5, #77	; 0x4d
 80011d2:	35ff      	adds	r5, #255	; 0xff
 80011d4:	50d5      	str	r5, [r2, r3]
 80011d6:	e7c8      	b.n	800116a <__register_exitproc+0x2e>
 80011d8:	f000 f816 	bl	8001208 <__retarget_lock_release_recursive>
 80011dc:	2001      	movs	r0, #1
 80011de:	4240      	negs	r0, r0
 80011e0:	e7d2      	b.n	8001188 <__register_exitproc+0x4c>
 80011e2:	46c0      	nop			; (mov r8, r8)
 80011e4:	20000438 	.word	0x20000438
 80011e8:	08001264 	.word	0x08001264

080011ec <register_fini>:
 80011ec:	4b03      	ldr	r3, [pc, #12]	; (80011fc <register_fini+0x10>)
 80011ee:	b510      	push	{r4, lr}
 80011f0:	2b00      	cmp	r3, #0
 80011f2:	d002      	beq.n	80011fa <register_fini+0xe>
 80011f4:	4802      	ldr	r0, [pc, #8]	; (8001200 <register_fini+0x14>)
 80011f6:	f7ff ff0f 	bl	8001018 <atexit>
 80011fa:	bd10      	pop	{r4, pc}
 80011fc:	00000000 	.word	0x00000000
 8001200:	08001029 	.word	0x08001029

08001204 <__retarget_lock_acquire_recursive>:
 8001204:	4770      	bx	lr
 8001206:	46c0      	nop			; (mov r8, r8)

08001208 <__retarget_lock_release_recursive>:
 8001208:	4770      	bx	lr
 800120a:	46c0      	nop			; (mov r8, r8)

0800120c <_init>:
 800120c:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 800120e:	46c0      	nop			; (mov r8, r8)
 8001210:	bcf8      	pop	{r3, r4, r5, r6, r7}
 8001212:	bc08      	pop	{r3}
 8001214:	469e      	mov	lr, r3
 8001216:	4770      	bx	lr

08001218 <_fini>:
 8001218:	b5f8      	push	{r3, r4, r5, r6, r7, lr}
 800121a:	46c0      	nop			; (mov r8, r8)
 800121c:	bcf8      	pop	{r3, r4, r5, r6, r7}
 800121e:	bc08      	pop	{r3}
 8001220:	469e      	mov	lr, r3
 8001222:	4770      	bx	lr
