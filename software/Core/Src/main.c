/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lvgl.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
DAC_HandleTypeDef hdac1;
DMA_HandleTypeDef hdma_dac1_ch1;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim6;

/* USER CODE BEGIN PV */

#define MAX_SAMPLES 1024
#define CYCLES_PER_SECOND (64000000UL / MAX_SAMPLES)
#define COUNTS_PER_VOLT 4095/131;

uint32_t ch1_samples[MAX_SAMPLES] = { 0 };

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_SPI1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM6_Init(void);
static void MX_DAC1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

#define DISPLAY_WIDTH 320
#define DISPLAY_HEIGHT 240
#define DISPLAY_PIXELS (DISPLAY_WIDTH * DISPLAY_HEIGHT)
#define DISPLAY_BUFFER_SIZE (DISPLAY_WIDTH*10)

#define DISPLAY_TX_BYTES_GROUPING 320

//#define IMAGE_WIDTH 128
//#define IMAGE_HEIGHT 128
//#define IMAGE_PIXELS (IMAGE_WIDTH * IMAGE_HEIGHT)

//uint8_t image_half_pixels[IMAGE_PIXELS*2] = { 0 };

static lv_disp_buf_t  lvgl_display_buffers;
static lv_color_t lvgl_display_buffer[DISPLAY_BUFFER_SIZE];

void write_display(uint8_t cmd, uint8_t *data, size_t len) {
    HAL_GPIO_WritePin(DISP_CS_GPIO_Port, DISP_CS_Pin, GPIO_PIN_RESET);

    HAL_GPIO_WritePin(DISP_DC_GPIO_Port, DISP_DC_Pin, GPIO_PIN_RESET);
    HAL_SPI_Transmit(&hspi1, &cmd, 1, 100);

    for (size_t i = 0; i < len; i += 1) {
        HAL_GPIO_WritePin(DISP_DC_GPIO_Port, DISP_DC_Pin, GPIO_PIN_SET);
        HAL_SPI_Transmit(&hspi1, &data[i], 1, 100);
    }

    HAL_GPIO_WritePin(DISP_CS_GPIO_Port, DISP_CS_Pin, GPIO_PIN_SET);
}

void init_display() {
    HAL_GPIO_WritePin(DISP_RST_GPIO_Port, DISP_RST_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(DISP_CS_GPIO_Port, DISP_CS_Pin, GPIO_PIN_SET);
    HAL_Delay(100);
    HAL_GPIO_WritePin(DISP_RST_GPIO_Port, DISP_RST_Pin, GPIO_PIN_RESET);
    HAL_Delay(100);
    HAL_GPIO_WritePin(DISP_RST_GPIO_Port, DISP_RST_Pin, GPIO_PIN_SET);
    HAL_Delay(100);

    // SWRESET: soft reset
    write_display(0x01, 0, 0);

    // SLPOUT: disable sleep
    write_display(0x11, 0, 0);
    write_display(0x20, 0, 0);

    // COLMOD: 16 bits per pixel mode
    uint8_t colmod_param = 0x05;
    write_display(0x3A, &colmod_param, 1);

    // MADCTL: Set vertical mode
    uint8_t madctl_param = 0b01100000;
    write_display(0x36, &madctl_param, 1);

    // NORON: normal mode (partial off)
    write_display(0x13, 0, 0);

    // DISPON: display on
    write_display(0x29, 0, 0);
}

size_t min(size_t a, size_t b) {
    if (a < b) {
        return a;
    } else {
        return b;
    }
}

void lvgl_flush_callback(lv_disp_drv_t * disp_drv, const lv_area_t * area, lv_color_t * color_p) {
    //HAL_GPIO_WritePin(LED_CH2_EN_GPIO_Port, LED_CH2_EN_Pin, GPIO_PIN_SET);
    uint8_t caset_param[4] = { area->x1 >> 8, area->x1 & 0xFF, area->x2 >> 8, area->x2 & 0xFF};
    write_display(0x2A, (uint8_t *)caset_param, 4);

    uint8_t raset_param[4] = { area->y1 >> 8, area->y1 & 0xFF, area->y2 >> 8, area->y2 & 0xFF};
    write_display(0x2B, (uint8_t *)raset_param, 4);

    HAL_GPIO_WritePin(DISP_CS_GPIO_Port, DISP_CS_Pin, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(DISP_DC_GPIO_Port, DISP_DC_Pin, GPIO_PIN_RESET);

    uint8_t ramwr_cmd = 0x2C;
    HAL_SPI_Transmit(&hspi1, &ramwr_cmd, 1, 100);

    HAL_GPIO_WritePin(DISP_DC_GPIO_Port, DISP_DC_Pin, GPIO_PIN_SET);

    size_t number_bytes = (area->x2 - area->x1 + 1) * (area->y2 - area->y1 + 1) * 2;

    for (size_t start_byte = 0; start_byte < number_bytes; start_byte += DISPLAY_TX_BYTES_GROUPING) {
        size_t group_length = min(DISPLAY_TX_BYTES_GROUPING, number_bytes-start_byte);
        if (group_length > 0) {
            HAL_SPI_Transmit(&hspi1, &((uint8_t *) color_p)[start_byte], group_length, 100);
        }
    }

    write_display(0x00, 0, 0);

    lv_disp_flush_ready(disp_drv);
    //HAL_GPIO_WritePin(LED_CH2_EN_GPIO_Port, LED_CH2_EN_Pin, GPIO_PIN_RESET);
}

bool lvgl_encoder_read(lv_indev_drv_t * drv, lv_indev_data_t  * data) {
    int16_t count = (int16_t) TIM2->CNT;
    int16_t ticks = count / 2;
    data->enc_diff = -ticks;
    TIM2->CNT = count - ticks * 2;

    GPIO_PinState enter_pressed = HAL_GPIO_ReadPin(BTN_ENTER_GPIO_Port, BTN_ENTER_Pin);

    if (enter_pressed == GPIO_PIN_RESET) {
        data->state = LV_INDEV_STATE_PR;
    } else {
        data->state = LV_INDEV_STATE_REL;
    }

    return false;
}

bool lvgl_keypad_read(lv_indev_drv_t * drv, lv_indev_data_t * data) {
    GPIO_PinState back_pressed = HAL_GPIO_ReadPin(BTN_BACK_GPIO_Port, BTN_BACK_Pin);
    data->key = LV_KEY_ESC;

    if (back_pressed == GPIO_PIN_RESET) {
        data->state = LV_INDEV_STATE_PR;
        HAL_GPIO_WritePin(LED_CH2_EN_GPIO_Port, LED_CH2_EN_Pin, GPIO_PIN_SET);
    } else {
        data->state = LV_INDEV_STATE_REL;
        HAL_GPIO_WritePin(LED_CH2_EN_GPIO_Port, LED_CH2_EN_Pin, GPIO_PIN_RESET);
    }

    return false;
}

static void event_handler(lv_obj_t * obj, lv_event_t event)
{
    if(event == LV_EVENT_CLICKED) {
        //printf("Clicked\n");
    }
    else if(event == LV_EVENT_VALUE_CHANGED) {
        //printf("Toggled\n");
    }
}

static lv_obj_t * function_dropdown;
static lv_obj_t * min_peak_spinbox;
static lv_obj_t * max_peak_spinbox;
static lv_obj_t * frequency_spinbox;
static lv_obj_t * symmetry_spinbox;

void regenerate_samples_from_ui();

void on_ui_changed(lv_obj_t * obj, lv_event_t event) {
    if (event == LV_EVENT_VALUE_CHANGED) {
        regenerate_samples_from_ui();
    }
}

void lv_create_ui(void) {

    lv_group_t* g = lv_group_create();

    lv_indev_t* cur_drv = NULL;
    for (;;) {
        cur_drv = lv_indev_get_next(cur_drv);
        if (!cur_drv) {
            break;
        }

        if (cur_drv->driver.type == LV_INDEV_TYPE_ENCODER) {
            lv_indev_set_group(cur_drv, g);
        }
    }

    function_dropdown = lv_dropdown_create(lv_scr_act(), NULL);
    lv_obj_align(function_dropdown, NULL, LV_ALIGN_IN_TOP_LEFT, 80, 0);
    lv_obj_set_width(function_dropdown, 80);
    lv_obj_set_event_cb(function_dropdown, on_ui_changed);
    lv_dropdown_set_options(function_dropdown, "Square\nTriangle\nSine");

    lv_obj_t * function_label = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(function_label, "Function");
    lv_obj_align(function_label, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 0);

    max_peak_spinbox = lv_spinbox_create(lv_scr_act(), NULL);
    lv_obj_align(max_peak_spinbox, NULL, LV_ALIGN_IN_TOP_LEFT, 80, 30);
    lv_obj_set_width(max_peak_spinbox, 80);
    lv_obj_set_event_cb(max_peak_spinbox, on_ui_changed);
    lv_spinbox_set_value(max_peak_spinbox, 120);
    lv_spinbox_set_range(max_peak_spinbox, 0, 120);
    lv_spinbox_step_prev(max_peak_spinbox);
    lv_spinbox_set_digit_format(max_peak_spinbox, 3, 2);
    lv_textarea_set_cursor_blink_time(max_peak_spinbox, 0);

    lv_obj_t * max_peak_label = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(max_peak_label, "Max Peak");
    lv_obj_align(max_peak_label, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 30);

    min_peak_spinbox = lv_spinbox_create(lv_scr_act(), NULL);
    lv_obj_align(min_peak_spinbox, NULL, LV_ALIGN_IN_TOP_LEFT, 80, 60);
    lv_obj_set_width(min_peak_spinbox, 80);
    lv_obj_set_event_cb(min_peak_spinbox, on_ui_changed);
    lv_spinbox_set_value(min_peak_spinbox, 0);
    lv_spinbox_set_range(min_peak_spinbox, 0, 120);
    lv_spinbox_step_prev(min_peak_spinbox);
    lv_spinbox_set_digit_format(min_peak_spinbox, 3, 2);
    lv_textarea_set_cursor_blink_time(min_peak_spinbox, 0);

    lv_obj_t * min_peak_label = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(min_peak_label, "Min Peak");
    lv_obj_align(min_peak_label, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 60);

    frequency_spinbox = lv_spinbox_create(lv_scr_act(), NULL);
    lv_obj_align(frequency_spinbox, NULL, LV_ALIGN_IN_TOP_LEFT, 80, 90);
    lv_obj_set_width(frequency_spinbox, 80);
    lv_obj_set_event_cb(frequency_spinbox, on_ui_changed);
    lv_spinbox_set_value(frequency_spinbox, 10);
    lv_spinbox_set_range(frequency_spinbox, 0, 1000000);
    lv_spinbox_step_prev(frequency_spinbox);
    lv_spinbox_set_digit_format(frequency_spinbox, 6, 6);
    lv_textarea_set_cursor_blink_time(frequency_spinbox, 0);

    lv_obj_t * frequency_label = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(frequency_label, "Frequency");
    lv_obj_align(frequency_label, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 90);

    symmetry_spinbox = lv_spinbox_create(lv_scr_act(), NULL);
    lv_obj_align(symmetry_spinbox, NULL, LV_ALIGN_IN_TOP_LEFT, 80, 120);
    lv_obj_set_width(symmetry_spinbox, 80);
    lv_obj_set_event_cb(symmetry_spinbox, on_ui_changed);
    lv_spinbox_set_value(symmetry_spinbox, 50);
    lv_spinbox_set_range(symmetry_spinbox, 0, 100);
    lv_spinbox_step_prev(symmetry_spinbox);
    lv_spinbox_set_digit_format(symmetry_spinbox, 3, 3);
    lv_textarea_set_cursor_blink_time(symmetry_spinbox, 0);

    lv_obj_t * symmetry_label = lv_label_create(lv_scr_act(), NULL);
    lv_label_set_text(symmetry_label, "Symmetry");
    lv_obj_align(symmetry_label, NULL, LV_ALIGN_IN_TOP_LEFT, 0, 120);

    lv_group_add_obj(g, function_dropdown);
    lv_group_add_obj(g, max_peak_spinbox);
    lv_group_add_obj(g, min_peak_spinbox);
    lv_group_add_obj(g, frequency_spinbox);
    lv_group_add_obj(g, symmetry_spinbox);
}

// Generate a single cycle of a square wave, fit into `len` samples,
// with the center 'len*symmetry/100' samples in, and
// with a max value of `peak_max` and a min value of `peak_min`
void generate_square(uint32_t samples[MAX_SAMPLES], size_t len_halfs, size_t symmetry, uint32_t peak_max, uint32_t peak_min) {
    size_t len = MAX_SAMPLES >> len_halfs;
    size_t mid = len * symmetry / 100;
    for (size_t i = 0; i < mid; i += 1){
        samples[i] = peak_max;
    }
    for (size_t i = mid; i < len; i += 1) {
        samples[i] = peak_min;
    }
}

void generate_triangle(uint32_t samples[MAX_SAMPLES], size_t len_halfs, size_t symmetry, uint32_t peak_max, uint32_t peak_min) {
    size_t len = MAX_SAMPLES >> len_halfs;
    size_t first_quarter = len / 2 * symmetry / 100;
    size_t second_quarter = len / 2 - first_quarter;
    uint32_t amplitude = peak_max - peak_min;

    for (int i = 0; i < first_quarter; i += 1) {
        samples[i] = i * amplitude / (first_quarter * 2) + peak_min + amplitude / 2;
    }

    for (int i = 0; i < second_quarter * 2; i += 1) {
        samples[i + first_quarter] = peak_max - i * amplitude / (second_quarter * 2);
    }

    for (int i = 0; i < first_quarter; i += 1) {
        samples[i + first_quarter + second_quarter * 2] = i * amplitude / (first_quarter * 2) + peak_min;
    }
}

// cycles / second => MAX_SAMPLES / cycle
// second => MAX_SAMPLES
// second * samples / second * 1 / MAX_SAMPLES
// cycles / second * seconds / base_cycle

void regenerate_samples_from_ui() {
    if (
            function_dropdown != NULL &&
            max_peak_spinbox != NULL &&
            min_peak_spinbox != NULL &&
            frequency_spinbox != NULL &&
            symmetry_spinbox != NULL
    ) {
        uint16_t function = lv_dropdown_get_selected(function_dropdown);
        uint32_t max_peak = lv_spinbox_get_value(max_peak_spinbox);
        uint32_t min_peak = lv_spinbox_get_value(min_peak_spinbox);
        uint32_t frequency = lv_spinbox_get_value(frequency_spinbox);
        uint32_t symmetry = lv_spinbox_get_value(symmetry_spinbox);

        uint32_t max_peak_counts = max_peak * COUNTS_PER_VOLT;
        uint32_t min_peak_counts = min_peak * COUNTS_PER_VOLT;
        uint32_t timer_period = CYCLES_PER_SECOND / frequency;

        HAL_DAC_Stop_DMA(&hdac1, DAC_CHANNEL_1);

        switch (function) {
            case 0:
                generate_square(ch1_samples, 0, symmetry, max_peak_counts, min_peak_counts);
                break;
            case 1:
                generate_triangle(ch1_samples, 0, symmetry, max_peak_counts, min_peak_counts);
                break;
            default:
                //memset(&ch1_samples, 0, MAX_SAMPLES);
                break;
        }

        TIM6->ARR = timer_period;

        HAL_GPIO_WritePin(CH1_DIV1_GPIO_Port, CH1_DIV1_Pin, GPIO_PIN_RESET);
        HAL_GPIO_WritePin(CH1_DIV2_GPIO_Port, CH1_DIV2_Pin, GPIO_PIN_RESET);

        HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_1, (uint32_t *) ch1_samples, MAX_SAMPLES, DAC_ALIGN_12B_R);
    }
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI1_Init();
  MX_TIM6_Init();
  MX_DAC1_Init();
  /* USER CODE BEGIN 2 */
    MX_TIM2_Init();

    HAL_DAC_Start_DMA(&hdac1, DAC_CHANNEL_1, (uint32_t *) ch1_samples, 32, DAC_ALIGN_12B_R);
    HAL_TIM_Base_Start(&htim6);

    lv_init();

    init_display();
    HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);

    lv_disp_buf_init(&lvgl_display_buffers, lvgl_display_buffer, NULL, DISPLAY_BUFFER_SIZE);
    lv_disp_drv_t  lvgl_disp_drv;
    lv_disp_drv_init(&lvgl_disp_drv);
    lvgl_disp_drv.buffer = &lvgl_display_buffers;
    lvgl_disp_drv.flush_cb = lvgl_flush_callback;
    lv_disp_drv_register(&lvgl_disp_drv);

    lv_indev_drv_t lvgl_encoder_drv;
    lv_indev_drv_init(&lvgl_encoder_drv);
    lvgl_encoder_drv.type = LV_INDEV_TYPE_ENCODER;
    lvgl_encoder_drv.read_cb = lvgl_encoder_read;
    lv_indev_drv_register(&lvgl_encoder_drv);

    lv_indev_drv_t lvgl_keypad_drv;
    lv_indev_drv_init(&lvgl_keypad_drv);
    lvgl_keypad_drv.type = LV_INDEV_TYPE_KEYPAD;
    lvgl_keypad_drv.read_cb = lvgl_keypad_read;
    lv_indev_drv_register(&lvgl_keypad_drv);

    lv_create_ui();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
#pragma clang diagnostic push
#pragma ide diagnostic ignored "EndlessLoop"
    while (1) {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
        HAL_Delay(1);
        lv_tick_inc(1);
        lv_task_handler();
        HAL_GPIO_TogglePin(GPIOA, GPIO_PIN_12);
    }
#pragma clang diagnostic pop
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSIDiv = RCC_HSI_DIV1;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV1;
  RCC_OscInitStruct.PLL.PLLN = 8;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief DAC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_DAC1_Init(void)
{

  /* USER CODE BEGIN DAC1_Init 0 */

  /* USER CODE END DAC1_Init 0 */

  DAC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN DAC1_Init 1 */

  /* USER CODE END DAC1_Init 1 */
  /** DAC Initialization
  */
  hdac1.Instance = DAC1;
  if (HAL_DAC_Init(&hdac1) != HAL_OK)
  {
    Error_Handler();
  }
  /** DAC channel OUT1 config
  */
  sConfig.DAC_SampleAndHold = DAC_SAMPLEANDHOLD_DISABLE;
  sConfig.DAC_Trigger = DAC_TRIGGER_T6_TRGO;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;
  sConfig.DAC_ConnectOnChipPeripheral = DAC_CHIPCONNECT_DISABLE;
  sConfig.DAC_UserTrimming = DAC_TRIMMING_FACTORY;
  if (HAL_DAC_ConfigChannel(&hdac1, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN DAC1_Init 2 */

  /* USER CODE END DAC1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_Encoder_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 4294967295;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI1;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV2;
  sConfig.IC1Filter = 0;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV2;
  sConfig.IC2Filter = 0;
  if (HAL_TIM_Encoder_Init(&htim2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 0;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 0xffff;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void)
{

  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel2_3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel2_3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel2_3_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, DISP_RST_Pin|DISP_CS_Pin|DISP_DC_Pin|GPIO_PIN_8
                          |GPIO_PIN_9|GPIO_PIN_11|GPIO_PIN_12|LED_CH1_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_0|GPIO_PIN_1|LED_CH2_EN_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CH1_DIV1_GPIO_Port, CH1_DIV1_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CH1_DIV2_GPIO_Port, CH1_DIV2_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : BTN_BACK_Pin BTN_ENTER_Pin BTN_CH2_SEL_Pin BTN_CH2_EN_Pin
                           BTN_CH1_EN_Pin BTN_CH1_SEL_Pin */
  GPIO_InitStruct.Pin = BTN_BACK_Pin|BTN_ENTER_Pin|BTN_CH2_SEL_Pin|BTN_CH2_EN_Pin
                          |BTN_CH1_EN_Pin|BTN_CH1_SEL_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : DISP_RST_Pin DISP_CS_Pin DISP_DC_Pin PA8
                           PA9 PA11 PA12 LED_CH1_EN_Pin */
  GPIO_InitStruct.Pin = DISP_RST_Pin|DISP_CS_Pin|DISP_DC_Pin|GPIO_PIN_8
                          |GPIO_PIN_9|GPIO_PIN_11|GPIO_PIN_12|LED_CH1_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PB0 PB1 LED_CH2_EN_Pin */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|LED_CH2_EN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : CH1_DIV1_Pin */
  GPIO_InitStruct.Pin = CH1_DIV1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(CH1_DIV1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : CH1_DIV2_Pin */
  GPIO_InitStruct.Pin = CH1_DIV2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(CH1_DIV2_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
