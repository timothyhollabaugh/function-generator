/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32g0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BTN_BACK_Pin GPIO_PIN_9
#define BTN_BACK_GPIO_Port GPIOB
#define ENC_B_Pin GPIO_PIN_0
#define ENC_B_GPIO_Port GPIOA
#define ENC_A_Pin GPIO_PIN_1
#define ENC_A_GPIO_Port GPIOA
#define DISP_RST_Pin GPIO_PIN_2
#define DISP_RST_GPIO_Port GPIOA
#define DISP_CS_Pin GPIO_PIN_3
#define DISP_CS_GPIO_Port GPIOA
#define DISP_SCK_Pin GPIO_PIN_5
#define DISP_SCK_GPIO_Port GPIOA
#define DISP_DC_Pin GPIO_PIN_6
#define DISP_DC_GPIO_Port GPIOA
#define DISP_MOSI_Pin GPIO_PIN_7
#define DISP_MOSI_GPIO_Port GPIOA
#define CH1_DIV1_Pin GPIO_PIN_6
#define CH1_DIV1_GPIO_Port GPIOC
#define CH1_DIV2_Pin GPIO_PIN_10
#define CH1_DIV2_GPIO_Port GPIOA
#define LED_CH1_EN_Pin GPIO_PIN_15
#define LED_CH1_EN_GPIO_Port GPIOA
#define LED_CH2_EN_Pin GPIO_PIN_3
#define LED_CH2_EN_GPIO_Port GPIOB
#define BTN_ENTER_Pin GPIO_PIN_4
#define BTN_ENTER_GPIO_Port GPIOB
#define BTN_CH2_SEL_Pin GPIO_PIN_5
#define BTN_CH2_SEL_GPIO_Port GPIOB
#define BTN_CH2_EN_Pin GPIO_PIN_6
#define BTN_CH2_EN_GPIO_Port GPIOB
#define BTN_CH1_EN_Pin GPIO_PIN_7
#define BTN_CH1_EN_GPIO_Port GPIOB
#define BTN_CH1_SEL_Pin GPIO_PIN_8
#define BTN_CH1_SEL_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
